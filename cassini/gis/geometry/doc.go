/*
The base Geometry class has subclasses for Point, Curve, Surface and
GeometryCollection. Each geometric object is associated with a Spatial Reference
System, which describes the coordinate space in which the geometric object is
defined.

	WARNING : In Cassini Geometry, Curve and Surface are abstract classes.
	Cassini implements following object : Point, LineString and Polygon.
	Notes that specifics objects Line and Ring are particulars LineString,
	and Triangle is particular Polygon.

The extended Geometry model has specialized 0, 1 and 2-dimensional collection
classes named MultiPoint, MultiLineString and MultiPolygon for modeling
geometries corresponding to collections of Points, LineStrings and Polygons,
respectively. MultiCurve and MultiSurface are introduced as abstract
superclasses that generalize the collection interfaces to handle Curves and
Surfaces.

Specifications

OpenGIS ® Implementation Standard for Geographic information
- Simple feature access - Part 1: Common architecture

This implementation shall satisfy the requirements of one or more test suites
specified in the other parts of ISO 19125.


Geometry

Geometry is the root class of the hierarchy. Geometry is an abstract
(non-instantiable) class.

The instantiable subclasses of Geometry defined in this Standard are
restricted to 0, 1 and 2-dimensional geometric objects that exist in 2, 3 or
4-dimensional coordinate space (R 2 , R 3 or R 4). Geometry values in R 2
have points with coordinate values for x and y. Geometry values in R 3 have
points with coordinate values for x, y and z or for x, y and m. Geometry
values in R 4 have points with coordinate values for x, y, z and m. The
interpretation of the coordinates is subject to the coordinate reference
systems associated to the point. All coordinates within a geometry object
should be in the same coordinate reference systems. Each coordinate shall
be unambiguously associated to a coordinate reference system either directly
or through its containing geometry.

The z coordinate of a point is typically, but not necessarily, represents
altitude or elevation. The m coordinate represents a measurement.

All Geometry classes described in this standard are defined so that instances
of Geometry are topologically closed, i.e. all represented geometries include
their boundary as point sets. This does not affect their

Basic methods on geometric objects

Dimension() int, returns the inherent dimension of this geometric object,
which must be less than or equal to the coordinate dimension. In
non-homogeneous collections, this will return the largest topological
dimension of the contained objects.

GeometryType() string, returns the name of the instantiable subtype of
Geometry of which this geometric object is an instantiable member. The
name of the subtype of Geometry is returned as a string.

SRID() int, returns the Spatial Reference System ID for this
geometric object. This will normally be a foreign key to an index of
reference systems stored in either the same or some other datastore.

	WARNING : Cassini don't implments this methods, please get SIRD var in each

Envelope() Polygon, return the minimum bounding box for this Geometry,
returned as a Polygon. The polygon is defined by the corner points of
of the bounding box
[(MINX, MINY), (MINX, MAXY), (MAXX, MINY), (MAXX, MINY), (MINX, MINY)]
Minimums for Z and M may be added. The simplest representation of an
Envelope is as two direct positions, one containing all the minimums,
and another all the maximums. In some cases, this coordinate will be
outside the range of validity for the Spatial Reference System.

AsText() string, returns the exports this geometric object to a specific
Well-knownText Representation of Geometry.

AsBinary() binary, returns exports this geometric object to a specific
Well-known Binary Representation of Geometry.

IsEmpty() int, returns true if this geometric object is the empty
Geometry. If true, then this geometric object represents the empty point
set ∅ for the coordinate space. The return type is integer, but is
interpreted as Boolean, TRUE=1, FALSE=0.

IsSimple() int, returns true if this geometric object has no
anomalous geometric points, such as self intersection or self tangency.
The description of each instantiable geometric class will include the
specific conditions that cause an instance of that class to be
classified as not simple. The return type is integer, but is interpreted
as Boolean, TRUE=1, FALSE=0.

Is3D() int, returns true if this geometric object has z coordinate values.

IsMeasured() int, returns true if this geometric object has m
coordinate values.

Boundary() Polygon, returns the closure of the combinatorial boundary
of this geometric object. Because the result of this function is a
closure, and hence topologically closed, the resulting boundary can be
represented using Polygon object.

Methods for testing spatial relations between geometric objects

The methods in this subclause are defined and described in more detail
following the description of the sub-types of Geometry. For each of the
following, the return type is integer, but is interpreted as Boolean, TRUE=1,
FALSE=0.

Equals(anotherGeometry: Geometry) int, returns true if this
geometric object is “spatially equal” to anotherGeometry.

Disjoint(anotherGeometry: Geometry) int, returns trueif this
geometric object is “spatially disjoint” from anotherGeometry.

Intersects(anotherGeometry: Geometry) int, returns true if this
geometric object “spatially intersects” anotherGeometry.

Touches(anotherGeometry: Geometry) int, returns true if this
geometric object “spatially touches” anotherGeometry.

Crosses(anotherGeometry: Geometry) int, returns true if this
geometric object “spatially crosses’ anotherGeometry.

Within(anotherGeometry: Geometry) int, returns true if this
geometric object is “spatially within” anotherGeometry.

Contains(anotherGeometry: Geometry) int, returns true if this
geometric object “spatially contains” anotherGeometry.

Overlaps(anotherGeometry: Geometry) int, returns true if this
geometric object “spatially overlaps” anotherGeometry.

Relate(anotherGeometry: Geometry, intersectionPatternMatrix: string):
int, returns true if this eometric object is spatially related
to anotherGeometry by testing for intersections between the interior,
boundary and exterior of the two geometric objects as specified by the
values in the intersectionPatternMatrix. This returns FALSE if all the
tested intersections are empty except exterio (this) intersect exterior
(another).

LocateAlong(mValue: float64) Geometry, returns a derived geometry
collection value that matches the specified m coordinate value. See
Subclause 6.1.2.6 “Measures on Geometry” for more details.

LocateBetween(mStart: float64, mEnd: float) Geometry, returns a derived
geometry collection value that matches the specified range of m
coordinate values inclusively. See Subclause 6.1.2.6 “Measures on
Geometry” for more details.

Methods that support spatial analysis

All of the following are geometric analysis and depend on the accuracy of the
coordinate representations and the limitations of linear interpolation in
this standard. The accuracy of the result at a fine level will be limited by
theseand related issues.

Distance (anotherGeometry: Geometry) float64, returns the shortest distance
between any two Points in the two geometric objects as calculated in the
spatial reference system of this geometric object. Because the
geometries are closed, it is possible to find a point on each geometric
object involved, such that the distance between these 2 points is the
returned distance between their geometric objects.

Buffer (distance: float64) Geometry, returns a geometric object that
represents all Points whose distance from this geometric object is less
than or equal to distance. Calculations are in the spatial reference
system of this geometric object. Because of the limitations of linear
interpolation, there will often be some relatively small error in this
distance, but it should be near the resolution of the coordinates used.

ConvexHull() Geometry, returns a geometric object that represents the
convex hull of this geometric object. Convex hulls, being dependent on
straight lines, can be accurately represented in linear interpolations
for any geometry restricted to linear interpolations.

Intersection (anotherGeometry: Geometry) Geometry, returns a geometric
object that represents the Point set intersection of this geometric
object with anotherGeometry.

Union (anotherGeometry: Geometry) Geometr, returns a geometric object that
represents the Point set union of this geometric object with
anotherGeometry.

Difference (anotherGeometry: Geometry) Geometry, rReturns a geometric object
that represents the Point set difference of this geometric object with
anotherGeometry.

SymDifference (anotherGeometry: Geometry) Geometry, rReturns a geometric
object that represents the Point set symmetric difference of this
geometric object with anotherGeometry.

Use of Z and M coordinate values

A Point value may include a z coordinate value. The z coordinate value
traditionally represents the third dimension (i.e. 3D). In a Geographic
Information System (GIS) this may be height above or below sea level. For
example: A map might have point identifying the position of a mountain peak
by its location on the earth, with the x and y coordinate values, and the
height of the mountain, with the z coordinate value.

A Point value may include an m coordinate value. The m coordinate value
allows the application environment to associate some measure with the point
values. For example: A stream network may be modeled as multilinestring
value with the m coordinate values measuring the distance from the mouth of
stream. The method LocateBetween may be used to find all the parts of the
stream that are between, for example, 10 and 12 kilometers from the mouth.
There are no constraints on the m coordinate values in a Geometry (e.g., the
m coordinate values do not have to be continually increasing along a
LineString value).

Observer methods returning Point values include z and m coordinate values
when they are present.

Spatial operations work in the "map geometry" of the data and will therefore
not reflect z or m values in calculations (e.g., Equals, Length) or in
generation of new geometry values (e.g., Buffer, ConvexHull, Intersection).
This is done by projecting the geometric objects onto the horizontal plane to
obtain a "footprint" or "shadow" of the objects for the purposed of map
calculations. In other words, it is possible to store and obtain z (and m)
coordinate values but they are ignored in all other operations which are
based on map geometries. Implementations are free to include true 3D
geometric operations, but should be consistent with ISO 19107..

Measures on Geometry

The LocateAlong and LocateBetween methods derive MultiPoint or MultiCurve
values from the given geometry that match a measure or a specific range of
measures from the start measure to the end measure. The LocateAlong method
is a variation of the LocateBetween method where the start measure and end
measure are equal. (See SQL/MM [1])

Zero-dimensional geometry values

Only points in the 0-dimensional geometry values with m coordinate values
between SM and EM inclusively are returned as multipoint value. If no matching m
coordinate values are found, then an empty set of type Point is returned.

For example:

a) If LocateAlong is invoked with an M value of 4 on a MultiPoint value with
well-known text representation:
	multipoint m(1 0 4, 1 1 1, 1 2 2, 3 1 4, 5 3 4)
then the result is the following MultiPoint value with well-known text
representation:
	multipoint m(1 0 4, 3 1 4, 5 3 4)

b) If LocateBetween is invoked with an SM value of 2 and an EM value of 4 on a
MultiPoint value with well-known text representation:
	multipoint m(1 0 4, 1 1 1, 1 2 2, 3 1 4, 5 3 5, 9 5 3, 7 6 7)
then the result is the following MultiPoint value with well-known text
representation:
	multipoint m(1 0 4, 1 2 2, 3 1 4, 9 5 3)

c) If LocateBetween is invoked with an SM value of 1 and an EM value of 4 on a
Point value with well-known text representation:
	point m(7 6 7)
then the result is the following MultiPoint value with well-known text
representation:
	point m empty

d) If LocateBetween is invoked with an SM value of 7 and an EM value of 7 on a
Point value with well-known text representation:
	point m(7 6 7)
then the result is the following MultiPoint value with well-known text
representation:
	multipoint m(7 6 7)

One-dimensional geometry value

Interpolation is used to determine any points on the 1-dimensional geometry with
an m coordinate value between mStart and mEnd inclusively. The
implementation-defined interpolation algorithm is used to estimate values
between measured values, usually using a mathematical function. The
interpolation is within a Curve element and not across Curve elements in a
MultiCurve. For example, given a measure of 6 and a 2-point LineString where the
m coordinate value of start point is 4 and the m coordinate value of the end
point is 8, since 6 is halfway between 4 and 8, the interpolation algorithm
would be a point on the LineString halfway between the start and end points.

The results are produced in a geometry collection. If there are consecutive
points in the 1-dimensional geometry with an m coordinate value between mStart
and mEnd inclusively, then a curve value element is added to the geometry
collection to represent the curve elements between these consecutive points. Any
disconnected points in the 1-dimensional geometry values with m coordinate
values between mStart and mEnd inclusively are also added to the geometry
collection. If no matching m coordinate values are found, then an empty set of
type ST_Point is returned.

For example:

a) If LocateAlong is invoked with an M value of 4 on a LineString value with
well-known text representation:
	LineStringM(1 0 0, 3 1 4, 5 3 4, 5 5 1, 5 6 4, 7 8 4, 9 9 0)
then the result is the following MultiLineString value with well-known text
representation:
	MultiLineStringM((3 1 4, 5 3 4), (5 6 4, 7 8 4))

b) If LocateBetween is invoked with an mStart value of 2 and an mend value of 4
on a LineString value with well-known text representation:
	LineStringM(1 0 0, 1 1 1, 1 2 2, 3 1 3, 5 3 4, 9 5 5, 7 6 6)
then the result is the following MultiLineString value with well-known text
representation:
	MultiLineStringM((1 2 2, 3 1 3, 5 3 4))

c) If LocateBetween is invoked with an SM value of 6 and an EM value of 9 on a
LineString value with well-known text representation:
	LineStringM(1 0 0, 1 1 1, 1 2 2, 3 1 3, 5 3 4, 9 5 5, 7 6 6)
then the result is the following MultiPoint value with well-known text
representation:
	MultiPointM(7 6 6)

d) If LocateBetween is invoked with an SM value of 2 and an EM value of 4 on a
MultiLineString value with well-known text representation:
	MultiLineStringM((1 0 0, 1 1 1, 1 2 2, 3 1 3), (4 5 3, 5 3 4, 9 5 5, 7 6 6))
then the result is the following MultiLineString value with well-known text
representation:
	MultiLineStringM((1 2 2, 3 1 3),(4 5 3, 5 3 4))

e) If LocateBetween is invoked with an SM value of 1 and an EM value of 3 on a
LineString value with well-known text representation:
	LineStringM(0 0 0, 2 2 2, 4 4 4)
then the result may be the following MultiLineString value with well-known text
representation:
	MultiLineStringM((1 1 1, 2 2 2, 3 3 3))

f) If LocateBetween is invoked with an SM value of 7 and an EM value of 9 on a
MultiLineString value with well-known text representation:
	MultiLineStringM((1 0 0, 1 1 1, 1 2 2, 3 1 3), (4 5 3, 5 3 4, 9 5 5, 7 6 6))

Modules

*/
package geometry
