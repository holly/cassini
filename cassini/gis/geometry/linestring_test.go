/*
linestring_test.go is part of Ccassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package geometry

import (
	"testing"
)

// -----------------------------------------------------------------------------
// Geometry interface implementation tests
// -----------------------------------------------------------------------------

func testLineStringImplementGeometry(t *testing.T) {
	var _ Geometry = (*LineString)(nil)
}

func testLineStringDimension(t *testing.T) {
	lsRef := &LineString{}
	if lsRef.Dimension() != 1 {
		t.Errorf(
			"LineString.Dimension() fail ! Result %s --> expected 1",
			lsRef.Dimension())
	}
}

func TestLineStringEquals(t *testing.T) {
	l := &LineString{Points: []Point{
		Point{X: 1, Y: 2},
		Point{X: 3, Y: 4},
		Point{X: 5, Y: 6, Z: 7, SRID: 8}}}
	testCases := []struct {
		n        int
		lsRef    *LineString
		g        Geometry
		expected bool
	}{
		// Other geometry
		{1, l, &Point{}, false},
		// Normal case
		{2, l, &LineString{
			Points: []Point{
				Point{X: 1, Y: 2},
				Point{X: 3, Y: 4},
				Point{X: 5, Y: 6, Z: 7, SRID: 8},
				Point{X: 999, Y: 888}}}, true},
		// One point with other SR
		{3, l, &LineString{
			Points: []Point{
				Point{X: 1, Y: 2},
				Point{X: 3, Y: 4},
				Point{X: 5, Y: 6, Z: 7, SRID: 4171},
				Point{X: 999, Y: 888}}}, false},
		// Two points inverted
		{3, l, &LineString{
			Points: []Point{
				Point{X: 3, Y: 4},
				Point{X: 1, Y: 2},
				Point{X: 5, Y: 6, Z: 7, SRID: 8},
				Point{X: 999, Y: 888}}}, false},
	}
	for _, tc := range testCases {
		if tc.lsRef.Equals(tc.g) != tc.expected {
			t.Errorf("LineString.Equals() test %d fail ! Result %s --> expected %s",
				tc.n,
				tc.lsRef.Equals(tc.g),
				tc.expected)
		}
	}
}

func testLineStringGeometryType(t *testing.T) {
	lsRef := &LineString{}
	if lsRef.GeometryType() != "LINETRING" {
		t.Errorf(
			"LineString.GeometryType() fail ! Result %s --> expected 'LINESTRING'",
			lsRef.GeometryType())
	}
}

func TestLineStringIs3D(t *testing.T) {
	testCases := []struct {
		n        int
		lsRef    *LineString
		expected bool
	}{
		{1, &LineString{Points: []Point{}}, false},

		{2, &LineString{Points: []Point{
			Point{X: 1, Y: 2}}}, false},

		{3, &LineString{Points: []Point{
			Point{X: 1, Y: 2},
			Point{X: 3, Y: 4},
			Point{X: 5, Y: 6, Z: 7, SRID: 8}}}, true},

		{4, &LineString{Points: []Point{
			Point{X: 1, Y: 2},
			Point{X: 3, Y: 4},
			Point{X: 5, Y: 6, M: 7, SRID: 8}}}, false},
	}
	for _, tc := range testCases {
		if tc.lsRef.Is3D() != tc.expected {
			t.Errorf("LineString.Is3D() test %d fail ! Result %s --> expected %s",
				tc.n,
				tc.lsRef.Is3D(),
				tc.expected)
		}
	}
}

func TestLineStringIsEmpty(t *testing.T) {
	testCases := []struct {
		n        int
		lsRef    *LineString
		expected bool
	}{
		{1, &LineString{Points: []Point{}}, true},

		{2, &LineString{Points: []Point{
			Point{X: 1, Y: 2}}}, false},

		{3, &LineString{Points: []Point{
			Point{X: 1, Y: 2},
			Point{X: 3, Y: 4},
			Point{X: 5, Y: 6, Z: 7, SRID: 8}}}, false},
	}
	for _, tc := range testCases {
		if tc.lsRef.IsEmpty() != tc.expected {
			t.Errorf("LineString.IsEmpty() test %d fail ! Result %s --> expected %s",
				tc.n,
				tc.lsRef.IsEmpty(),
				tc.expected)
		}
	}
}

func TestLineStringIsMeasured(t *testing.T) {
	testCases := []struct {
		n        int
		lsRef    *LineString
		expected bool
	}{
		{1, &LineString{Points: []Point{}}, false},

		{2, &LineString{Points: []Point{Point{X: 1, Y: 2}}}, false},

		{3, &LineString{Points: []Point{
			Point{X: 1, Y: 2},
			Point{X: 3, Y: 4},
			Point{X: 5, Y: 6, Z: 7, SRID: 8}}}, false},

		{4, &LineString{Points: []Point{
			Point{X: 1, Y: 2},
			Point{X: 3, Y: 4},
			Point{X: 5, Y: 6, M: 7, SRID: 8}}}, true},
	}
	for _, tc := range testCases {
		if tc.lsRef.IsMeasured() != tc.expected {
			t.Errorf("LineString.IsMeasured() test %d fail ! Result %s --> expected %s",
				tc.n,
				tc.lsRef.IsMeasured(),
				tc.expected)
		}
	}
}

func TestLineStringToWKT(t *testing.T) {
	testCases := []struct {
		n        int
		lsRef    *LineString
		expected string
	}{
		{1, &LineString{Points: []Point{}}, "LINESTRING EMPTY"},

		{2, &LineString{Points: []Point{Point{X: 1, Y: 2}}}, "LINESTRING (1 2)"},

		{3, &LineString{
			Points: []Point{
				Point{X: 1, Y: 2},
				Point{X: 3, Y: 4},
				Point{X: 5, Y: 6, Z: 7, SRID: 8}}},
			"LINESTRING Z (1 2 0, 3 4 0, 5 6 7)"},

		{4, &LineString{
			Points: []Point{
				Point{X: 1, Y: 2},
				Point{X: 3, Y: 4},
				Point{X: 5, Y: 6, M: 7, SRID: 8}}},
			"LINESTRING M (1 2 0, 3 4 0, 5 6 7)"},
	}
	for _, tc := range testCases {
		if tc.lsRef.ToWKT() != tc.expected {
			t.Errorf("LineString.ToWKT() test %d fail ! Result %s --> erxpected %s",
				tc.n,
				tc.lsRef.ToWKT(),
				tc.expected)
		}
	}
}

// -----------------------------------------------------------------------------
// Publics methods tests
// -----------------------------------------------------------------------------

func TestLineStringAppend(t *testing.T) {
	testCases := []struct {
		n        int
		lsRef    *LineString
		p        Point
		expected *LineString
	}{
		{1, &LineString{},
			Point{X: 1, Y: 2},
			&LineString{
				Points: []Point{
					Point{X: 1, Y: 2}}}},

		{2, &LineString{Points: []Point{
			Point{X: 1, Y: 2},
			Point{X: 3, Y: 4},
			Point{X: 5, Y: 6, Z: 7, SRID: 8}}},
			Point{X: 999, Y: 888},
			&LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8},
					Point{X: 999, Y: 888}}}},
	}
	for _, tc := range testCases {
		tc.lsRef.Append(tc.p)
		if !tc.lsRef.Equals(tc.expected) {
			t.Errorf("LineString.Append() test %d fail ! Result %s --> expected %s",
				tc.n,
				tc.lsRef.AsText(),
				tc.expected.AsText())
		}
	}
}

func TestLineStringEndPoint(t *testing.T) {
	testCases := []struct {
		n           int
		lsRef       *LineString
		expected    Point
		havingError bool
	}{
		{1, &LineString{Points: []Point{}}, Point{}, true},

		{2, &LineString{Points: []Point{Point{X: 1, Y: 2}}}, Point{X: 1, Y: 2}, false},

		{3, &LineString{Points: []Point{
			Point{X: 1, Y: 2},
			Point{X: 3, Y: 4},
			Point{X: 5, Y: 6, Z: 7, SRID: 8}}}, Point{X: 5, Y: 6, Z: 7, SRID: 8}, false},

		{4, &LineString{Points: []Point{
			Point{X: 1, Y: 2},
			Point{X: 3, Y: 4},
			Point{X: 5, Y: 6, M: 7, SRID: 8}}}, Point{X: 5, Y: 6, M: 7, SRID: 8}, false},
	}
	for _, tc := range testCases {
		pt, err := tc.lsRef.EndPoint()
		if pt != tc.expected && (err == nil) == tc.havingError {
			t.Errorf("LineString.EndPoint() test %d fail ! Result %s --> expected %s",
				pt.AsText(),
				tc.expected.AsText())
		}
	}
}

func TestLineStringInsert(t *testing.T) {
	l := &LineString{Points: []Point{
		Point{X: 1, Y: 2},
		Point{X: 3, Y: 4},
		Point{X: 5, Y: 6, Z: 7, SRID: 8}}}
	testCases := []struct {
		n           int
		lsRef       *LineString
		p           Point
		i           int
		expected    *LineString
		havingError bool
	}{
		// Negative index
		{1, l, Point{X: 999, Y: 888}, -5,
			&LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8}}},
			true},
		// Out of bound index
		{2, l, Point{X: 999, Y: 888}, 99998888,
			&LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8}}},
			true},
		// Index zero
		{3, l, Point{X: 999, Y: 888}, 0,
			&LineString{
				Points: []Point{
					Point{X: 999, Y: 888},
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8}}},
			false},
		// Bound limit
		{4, l, Point{X: 999, Y: 888}, 4,
			&LineString{
				Points: []Point{
					Point{X: 999, Y: 888},
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8},
					Point{X: 999, Y: 888}}},
			false},
		// In bound
		{5, l, Point{X: 999, Y: 888}, 2,
			&LineString{
				Points: []Point{
					Point{X: 999, Y: 888},
					Point{X: 1, Y: 2},
					Point{X: 999, Y: 888},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8},
					Point{X: 999, Y: 888},
				}},
			false},
	}
	for _, tc := range testCases {
		err := tc.lsRef.Insert(tc.p, tc.i)
		if (err != nil) != tc.havingError {
			t.Error("LineString.Insert() test %d fail ! It should return error.")
		}
		if !tc.lsRef.Equals(tc.expected) {
			t.Errorf("LineString.Insert() test %d fail ! Result %s --> expected %s",
				tc.n,
				tc.lsRef.AsText(),
				tc.expected.AsText())
		}
	}
}

func TestLineStringLength(t *testing.T) {
	testCases := []struct {
		n        int
		lsRef    *LineString
		expected float64
	}{
		{1, &LineString{Points: []Point{}}, 0},

		{2, &LineString{Points: []Point{Point{X: 1, Y: 2}}}, 0},

		{3, &LineString{Points: []Point{
			Point{X: 1, Y: 2},
			Point{X: 3, Y: 4},
			Point{X: 5, Y: 6, Z: 7, SRID: 8}}}, 5.656854249},

		{4, &LineString{Points: []Point{
			Point{X: 1, Y: 2},
			Point{X: 3, Y: 4},
			Point{X: 5, Y: 6, M: 7, SRID: 9},
			Point{X: 5, Y: 2},
			Point{X: 1, Y: 2}}}, 13.656854249},
	}
	for _, tc := range testCases {
		if tc.lsRef.Length() > tc.expected+epsilon &&
			tc.lsRef.Length() < tc.expected-epsilon {
			t.Errorf("LineString.Length() test %d fail !: Result %g --> expected %g +/- epsilon",
				tc.lsRef.Length(),
				tc.expected)
		}
	}
}

func TestLineStringPointN(t *testing.T) {
	lsEmpty := LineString{}
	ls := LineString{Points: []Point{
		Point{X: 5, Y: 6, M: 7, SRID: 8},
		Point{X: 3, Y: 4},
		Point{X: 5, Y: 6},
		Point{X: 7, Y: 8},
		Point{X: 1, Y: 2},
		Point{X: 5, Y: 6, M: 7, SRID: 8}}}
	// Empty LineString
	testCasesA := []struct {
		n           int
		i           int
		expected    Point
		havingError bool
	}{
		{1, -12, Point{}, true},
		{2, 12, Point{}, true},
		{3, 0, Point{}, false},
		{4, 3, Point{}, false},
		{5, 5, Point{}, false},
	}
	for _, tc := range testCasesA {
		pt, err := lsEmpty.PointN(tc.i)
		if pt != tc.expected && (err == nil) == tc.havingError {
			t.Errorf("LineString.PointN() test %d fail ! Result %s --> expected %s",
				tc.n,
				pt.AsText(),
				tc.expected.AsText())
		}
	}
	// Not empty LineString
	testCasesB := []struct {
		n           int
		i           int
		expected    Point
		havingError bool
	}{
		{1, -12, Point{}, true},
		{2, 12, Point{}, true},
		{3, 0, Point{X: 5, Y: 6, M: 7, SRID: 8}, false},
		{4, 3, Point{X: 7, Y: 8}, false},
		{5, 5, Point{X: 5, Y: 6, M: 7, SRID: 8}, false},
	}
	for _, tc := range testCasesB {
		pt, err := ls.PointN(tc.i)
		if pt != tc.expected && (err == nil) == tc.havingError {
			t.Errorf("LineString.PointN() test %d fail ! Result %s --> expected %s",
				tc.n,
				pt.AsText(),
				tc.expected.AsText())
		}
	}
}

func TestLineStringStartPoint(t *testing.T) {
	testCases := []struct {
		n           int
		lsRef       *LineString
		expected    Point
		havingError bool
	}{
		{1, &LineString{Points: []Point{}}, Point{}, true},
		{2, &LineString{Points: []Point{
			Point{X: 1, Y: 2}}}, Point{X: 1, Y: 2}, false},
		{3, &LineString{Points: []Point{
			Point{X: 1, Y: 2},
			Point{X: 3, Y: 4},
			Point{X: 5, Y: 6, Z: 7, SRID: 8}}}, Point{X: 5, Y: 6, Z: 7, SRID: 8}, false},
		{4, &LineString{Points: []Point{
			Point{X: 5, Y: 6, M: 7, SRID: 8},
			Point{X: 3, Y: 4},
			Point{X: 1, Y: 2}}}, Point{X: 5, Y: 6, M: 7, SRID: 8}, false},
	}
	for _, tc := range testCases {
		pt, err := tc.lsRef.StartPoint()
		if pt != tc.expected && (err == nil) == tc.havingError {
			t.Errorf("LineString.StartPoint() test %d fail ! Result %s --> expected %s",
				tc.n,
				pt.AsText(),
				tc.expected.AsText())
		}
	}
}
