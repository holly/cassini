/*
polygon_test.go is part of Cassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package geometry

import (
	"testing"
)

// Test interface implementation
func testPolygonImplementGeometry(t *testing.T) {
	var _ Geometry = (*Polygon)(nil)
}

func testPolygonDimension(t *testing.T) {
	pRef := &Polygon{}
	if pRef.Dimension() != 2 {
		t.Errorf(
			"Polygon.Dimension() fail ! Result %s --> expected 2",
			pRef.Dimension())
	}
}

func testPolygonEquals(t *testing.T) {
	p := &Polygon{
		ExteriorRing: LineString{
			Points: []Point{
				Point{X: 1, Y: 2},
				Point{X: 3, Y: 4},
				Point{X: 5, Y: 6, M: 7, SRID: 8}}},
		InteriorRings: []LineString{
			LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, SRID: 8}}},
			LineString{
				Points: []Point{
					Point{X: 8, Y: 8},
					Point{X: 9, Y: 9},
					Point{X: 10, Y: 10, SRID: 75}}}}}
	testCases := []struct {
		n        int
		pRef     *Polygon
		g        Geometry
		expected bool
	}{
		// Other geometry
		{1, p, &LineString{}, false},
		// Diff in exterior ring
		{2, p, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 99},
					Point{X: 5, Y: 6, M: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4},
						Point{X: 5, Y: 6, SRID: 8}}},
				LineString{
					Points: []Point{
						Point{X: 8, Y: 8},
						Point{X: 9, Y: 9},
						Point{X: 10, Y: 10, SRID: 75}}}}}, false},
		// Diff in interior ring
		{3, p, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, M: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 999},
						Point{X: 5, Y: 6, SRID: 8}}},
				LineString{
					Points: []Point{
						Point{X: 8, Y: 8},
						Point{X: 9, Y: 9},
						Point{X: 10, Y: 10, SRID: 75}}}}}, false},
		// Interior ring in other order
		{4, p, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, M: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 8, Y: 8},
						Point{X: 9, Y: 9},
						Point{X: 10, Y: 10, SRID: 75}}},
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 999},
						Point{X: 5, Y: 6, SRID: 8}}}}}, false},
		// Normal case
		{4, p, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, M: 7, SRID: 8}}},
			InteriorRings: []LineString{

				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 999},
						Point{X: 5, Y: 6, SRID: 8}}}, LineString{
					Points: []Point{
						Point{X: 8, Y: 8},
						Point{X: 9, Y: 9},
						Point{X: 10, Y: 10, SRID: 75}}}}}, true},
	}
	for _, tc := range testCases {
		if tc.pRef.Equals(tc.g) != tc.expected {
			t.Errorf("Polygon.Equals() test %d fail ! Result %s --> expected %s",
				tc.pRef.Equals(tc.g),
				tc.expected)
		}
	}

}

func testPolygonGeometryType(t *testing.T) {
	pRef := &Polygon{}
	if pRef.GeometryType() != "POLYGON" {
		t.Errorf(
			"Polygon.GeometryType() fail ! Result %s -è-> expected POLYGON",
			pRef.GeometryType())
	}
}

func TestPolygonIs3D(t *testing.T) {
	testCases := []struct {
		n        int
		pRef     *Polygon
		expected bool
	}{
		{1, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{}}}, false},
		{2, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2}}}}, false},
		{3, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4},
						Point{X: 5, Y: 6, SRID: 8}}}}}, true},
		{4, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, M: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4, Z: 999},
						Point{X: 5, Y: 6, SRID: 8}}}}}, true},
		{5, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, M: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4},
						Point{X: 5, Y: 6, SRID: 8}}}}}, false},
	}
	for _, tc := range testCases {
		if tc.pRef.Is3D() != tc.expected {
			t.Errorf("Polygon.Is3D() test %d fail ! Result %s --> expected %s",
				tc.pRef.Is3D(),
				tc.expected)
		}
	}
}

func TestPolygonIsEmpty(t *testing.T) {
	testCases := []struct {
		n        int
		pRef     *Polygon
		expected bool
	}{
		{1, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{}}}, true},
		{2, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2}}}}, false},
		{3, &Polygon{
			ExteriorRing: LineString{Points: []Point{
				Point{X: 1, Y: 2},
				Point{X: 3, Y: 4},
				Point{X: 5, Y: 6, Z: 7, SRID: 8}}}}, false},
	}
	for _, tc := range testCases {
		if tc.pRef.IsEmpty() != tc.expected {
			t.Errorf("Polygon.IsEmpty() test %d fail ! Result %s --> expected %s",
				tc.n,
				tc.pRef.IsEmpty(),
				tc.expected)
		}
	}
}

func TestPolygonIsMeasured(t *testing.T) {
	testCases := []struct {
		n        int
		pRef     *Polygon
		expected bool
	}{
		{1, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{}}}, false},
		{2, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2}}}}, false},
		{3, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8}}}}, false},
		{4, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, M: 7, SRID: 8}}}}, true},
		{5, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, M: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4},
						Point{X: 5, Y: 6, SRID: 8}}}}}, true},
		{6, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4, M: 999},
						Point{X: 5, Y: 6, SRID: 8}}}}}, true},
		{7, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4},
						Point{X: 5, Y: 6, SRID: 8}}}}}, false},
	}
	for _, tc := range testCases {
		if tc.pRef.IsMeasured() != tc.expected {
			t.Errorf("Polygon.IsMeasured() test %d fail ! Result %s --> expected %s",
				tc.pRef.IsMeasured(),
				tc.expected)
		}
	}
}

func TestPolygonToWKT(t *testing.T) {
	testCases := []struct {
		n        int
		pRef     *Polygon
		expected string
	}{
		{1, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{}}},
			"POLYGON EMPTY"},
		{2, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2}}}},
			"POLYGON ((1 2))"},
		{3, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8}}}},
			"POLYGON Z ((1 2 0, 3 4 0, 5 6 7))"},
		{4, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, M: 7, SRID: 8}}}},
			"POLYGON M ((1 2 0, 3 4 0, 5 6 7))"},
		{5, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, M: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4},
						Point{X: 5, Y: 6, SRID: 8}}}}},
			"POLYGON M ((1 2 0, 3 4 0, 5 6 7), (1 2 0, 3 4 0, 5 6 0))"},
		{6, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4, M: 999},
						Point{X: 5, Y: 6, SRID: 8}}}}},
			"POLYGON ZM ((1 2 0 0, 3 4 0 0, 5 6 7 0), (1 2 0 0, 3 4 0 999, 5 6 0 0))"},
		{7, &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4},
						Point{X: 5, Y: 6, SRID: 8}}}}},
			"POLYGON Z ((1 2 0, 3 4 0, 5 6 7), (1 2 0, 3 4 0, 5 6 0))"},
	}
	for _, tc := range testCases {
		if tc.pRef.ToWKT() != tc.expected {
			t.Errorf("Polygon.ToWKT() test %d fail ! Result %s --> expected %s",
				tc.n,
				tc.pRef.ToWKT(),
				tc.expected)
		}
	}
}
