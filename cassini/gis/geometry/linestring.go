/*
linestring.go is part of Cassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Module "ogeo.gis.geometry.linestring.go" contains code to implements Geometry
interface for geometry sub-class "LineString", code to export LineSTring in some
file format and code to create LineString (import) with some file format.
*/
package geometry

import (
	"errors"
	"fmt"
	"math"
)

/*
A LineString is a Curve with linear interpolation between Points.
each consecutive pair of Points defines a Line segment.

	WARNING : Cassini don't implements of curve class, but lineString have same
	interface ...

Curve

A Curve is a 1-dimensional geometric object usually stored as a sequence of
Points, with the subtype of Curve specifying the form of the interpolation
between Points. This standard defines only one subclass of Curve,
LineString, which uses linear interpolation between Points.

A Curve is a 1-dimensional geometric object that is the homeomorphic image of a
real, closed, interval, where n is the coordinate dimension of the underlying
Spatial Reference System.

A Curve is simple if it does not pass through the same Point twice with the
possible exception of the two end points (Reference [1], section 3.12.7.3):

A Curve is closed if its start Point is equal to its end Point (Reference [1],
section 3.12.7.3).

The boundary of a closed Curve is empty.

A Curve that is simple and closed is a Ring.

The boundary of a non-closed Curve consists of its two end Points (Reference
[1], section 3.12.3.2).

A Curve is defined as topologically closed, that is, it
contains its endpoints f(a) and f(b).

Curve implements following methods :

⎯ Length (): Double — The length of this Curve in its associated spatial
reference.

⎯ StartPoint (): Point — The start Point of this Curve.

⎯ EndPoint (): Point — The end Point of this Curve.

⎯ IsClosed (): Integer — Returns 1 (TRUE) if this Curve is closed
[StartPoint() = EndPoint()].

⎯ IsRing (): Integer — Returns 1 (TRUE) if this Curve is closed
[StartPoint() = EndPoint()] and this Curve is simple (does not pass through
the same Point more than once).

Linestring

A LineString is a Curve with linear interpolation between Points.
each consecutive pair of Points defines a Line segment.

A Line is a LineString with exactly 2 Points.

LineString implements following methods :

⎯ NumPoints ( ): Integer — The number of Points in this LineString.

⎯ PointN (N: Integer): Point — Returns the specified Point N in this LineString.

*/
type LineString struct {
	Points []Point
	SRID   int
}

// -----------------------------------------------------------------------------
// Geometry interface implementation
// -----------------------------------------------------------------------------

/*
AsText exports this LineString to a specific Well-knownText Representation
of this LineString (WKT). It returns a string.
*/
func (ls *LineString) AsText() string {
	return ls.ToWKT()
}

/*
Dimension gives the inherent dimension of this LineString, which must be less
than or equal to the coordinate dimension. A LineString is a 1-dimensional
geometric object that is the homeomorphic image of a real, closed, interval.
It returns constant 1.
*/
func (ls *LineString) Dimension() int {
	return 1
}

/*
Envelope, returns the bounding box for this Geometry, returned as a Polygon.

The polygon is defined by the corner points of of the bounding box
	[(MINX, MINY), (MINX, MAXY), (MAXX, MAXY), (MAXX, MINY)]

Minimums for Z and M may be added. The simplest representation of an
Envelope is as two direct positions, one containing all the minimums,
and another all the maximums. In some cases, this coordinate will be
outside the range of validity for the Spatial Reference System.

In case where this, LineString is empty it return empty Polygon and error.
*/

/*
Equals(anotherGeometry: Geometry) returns true if this other geometry is
LineString type, having same spatial reference, and points are equals in the
same sequence. Say geometric object is “spatially equal” to anotherGeometry.
*/
func (ls *LineString) Equals(g Geometry) bool {
	lsB, ok := (g.(*LineString))
	if !ok {
		return false
	}
	if ls.SRID != lsB.SRID {
		return false
	}
	for i, _ := range ls.Points {
		p := ls.Points[i]
		if !p.Equals(&lsB.Points[i]) {
			return false
		}
	}
	return true
}

/*func (ls *LineString) Envelope() (Polygon, error) {
	if len(ls.Points) < 2 {
		if ls.IsEmpty() {
			return Polygon{}, errors.New(
				"[Error from [cassini.gis.geometry.LineString] : " +
					"Empty linestring can not having envelope.")
		} else {
			return Polygon{}, errors.New(
				"[Error from [cassini.gis.geometry.LineString] : " +
					"Linestring with only one point can not having envelope.")
		}
	}
	p := Polygon{}
	minX := math.MaxFloat64
	maxX := math.MaxFloat64 * (-1)
	minY := math.MaxFloat64
	maxY := math.MaxFloat64 * (-1)
	p.SRID = ls.SRID
	for i, p := range ls.Points {
		minX = math.Min(minX, p.X)
		maxX = math.Max(maxX, p.X)
		minY = math.Min(minY, p.Y)
		maxY = math.Max(maxY, p.Y)
	}
	// FIX ME > Appending new point in linestring ...

	return p, nil
}*/

/*
GeometryType returns the name of the instantiable subtype of Geometry of which
this geometric object is an instantiable member. The name of the subtype of
Geometry is returned as a string.
*/
func (ls *LineString) GeometryType() string {
	return "LINESTRING"
}

/*
Is3D returns true if this LineString have one point in 3D.
*/
func (ls *LineString) Is3D() bool {
	for _, p := range ls.Points {
		if p.Is3D() {
			return true
		}
	}
	return false
}

/*
IsEmpty returns true if this LineString have zero point.
*/
func (ls *LineString) IsEmpty() bool {
	if len(ls.Points) == 0 {
		return true
	}
	return false
}

/*
IsMeasured returns true if this LineString have one point is measured.
*/
func (ls *LineString) IsMeasured() bool {
	for _, p := range ls.Points {
		if p.IsMeasured() {
			return true
		}
	}
	return false
}

/*
ToWKT exports this LineString to a specific Well-knownText Representation
of this µLineString (WKT). It returns a string.

*/
func (ls *LineString) ToWKT() string {
	if ls.IsEmpty() {
		return fmt.Sprint("LINESTRING EMPTY")
	}
	s := ls.subType()
	switch s {
	case "":
		return fmt.Sprintf(
			"LINESTRING %s",
			ls.innerWKT(s))
	case "Z":
		return fmt.Sprintf(
			"LINESTRING Z %s",
			ls.innerWKT(s))
	case "M":
		return fmt.Sprintf(
			"LINESTRING M %s",
			ls.innerWKT(s))
	case "ZM":
		return fmt.Sprintf(
			"LINESTRING ZM %s",
			ls.innerWKT(s))
	}
	return ""
}

// -----------------------------------------------------------------------------
// Publics methods
// -----------------------------------------------------------------------------

/*
Append inserts a new Point at ending position into Points list.
*/
func (ls *LineString) Append(p Point) {
	target := make([]Point, len(ls.Points)+1)
	copy(target, ls.Points)
	target[len(ls.Points)] = p
	ls.Points = target
}

/*
EndPoint returns the ending Point of this LineString. If this
LineString is empty, it returns empty Point and error. In others case Error
is NIL.
*/
func (ls *LineString) EndPoint() (Point, error) {
	if ls.IsEmpty() {
		return Point{}, errors.New(
			"Error from [cassini.gis.geometry.LineString.EndPoint] :  " +
				"Can't read ending point from empty linestring.")
	}
	return ls.Points[len(ls.Points)-1], nil
}

/*
Append inserts a new Point at index position into Points list. If insertion is
OK, it returns error equals to NIL. If index position is out of bound, it
returns error.

Notes that index start to zero.
*/
func (ls *LineString) Insert(p Point, i int) error {
	if i < 0 || i > len(ls.Points) {
		if i < 0 {
			return errors.New(
				"Error from [cassini.gis.geometry.LineString.Insert] : " +
					"Can't insert Point with negative index.")
		} else {
			return errors.New(
				"Error from [cassini.gis.geometry.LineString.Insert] : " +
					"Can't insert Point with index upper of bound.")
		}
	}
	target := make([]Point, len(ls.Points)+1)
	copy(target, ls.Points[:i])
	target[i] = p
	copy(target[i+1:], ls.Points[i:])
	ls.Points = target
	return nil
}

// FIX ME with intersects methods
// IsRing returns true if this LineString is closed [StartPoint = EndPoint]
// and this Curve is simple (does not pass through the same Point more
// than once).
/*func (ls *LineString) IsRing() bool {
	startPoint, err := ls.StartPoint()
	if err != nil {
		return false
	}
	endPoint, err := ls.EndPoint()
	if err != nil {
		return false
	}

	if Equals(Geometry(startPoint), Geometry(endPoint)) {
		return true
	}
	return false
}*/

/*
Length() float64, returns the length of this Curve in its associated spatial
reference. It returns 0 if linestring having less than 2 points.
*/
func (ls *LineString) Length() float64 {
	if len(ls.Points) < 2 {
		return 0
	}
	var l float64
	for i := 0; i < (len(ls.Points) - 1); i++ {
		pA, _ := ls.PointN(i)
		pB, _ := ls.PointN(i + 1)
		l += math.Hypot(pA.X-pB.X, pA.Y-pB.Y)
	}
	return l
}

/*
NumPoints returns the number of Points in this LineString.
*/
func (ls *LineString) NumPoints() int {
	return len(ls.Points)
}

/*
PointN returns the specified N Point from this LineString, with
N starting from 0. If this LineString is empty, it returns empty Point and
error. If N is out of index bound, it returns empty Point and error.
In others case, error is NIL.
*/
func (ls *LineString) PointN(n int) (Point, error) {
	if ls.IsEmpty() {
		return Point{}, errors.New(
			"Error from [cassini.gis.geometry.LineString.PointN] : " +
				"Can't read point from empty linestring.")
	}
	if n < 0 || n > (len(ls.Points)-1) {
		if n < 0 {
			return Point{}, errors.New(
				"Error from [cassini.gis.geometry.LineString.PointN] : " +
					"Can't read point with negative index.")
		} else {
			return Point{}, errors.New(
				"Error from [cassini.gis.geometry.LineString.PointN] : " +
					"Can't read point with N out of index bound.")
		}
	}
	return ls.Points[n], nil
}

/*
StartPoint returns the starting Point of this LineString. If this LineString
is empty, it returns empty Point and error. In others case Error is NIL.
*/
func (ls *LineString) StartPoint() (Point, error) {
	if ls.IsEmpty() {
		return Point{}, errors.New(
			"Error from [cassini.gis.geometry.LineString.EndPoint] : " +
				"Can't read starting point from empty linestring.")
	}
	return ls.Points[0], nil
}

// -----------------------------------------------------------------------------
// Privates methods
// -----------------------------------------------------------------------------

func (ls *LineString) subType() string {
	var k string
	if ls.Is3D() {
		k = "Z"
	}
	if ls.IsMeasured() {
		k += "M"
	}
	return k
}

func (ls *LineString) innerWKT(s string) string {
	var wkt string
	for i, p := range ls.Points {
		if i == 0 {
			wkt += "("
		} else {
			wkt += ", "
		}
		wkt += p.innerWKT(s)
	}
	wkt += ")"
	return wkt

}
