/*
example_point_test.go is part of Cassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Module "ogeo.gis.geometry.example_point_test.go" contains examples use to
documents point class int this package.
*/
package geometry

import (
	"fmt"
)

// -----------------------------------------------------------------------------
// Geometry interface implementation example
// -----------------------------------------------------------------------------

func ExamplePoint_AsText() {
	// Empty point
	pe := Point{}
	fmt.Println(pe.AsText())
	// 2D point
	p := Point{X: 2, Y: 5, SRID: 4171}
	fmt.Println(p.AsText())
	// 3D point
	pz := Point{X: 2, Y: 5, Z: 8, SRID: 4171}
	fmt.Println(pz.AsText())
	//Output:
	//POINT EMPTY
	//POINT (2 5)
	//POINT Z (2 5 8)
}

func ExamplePoint_Dimension() {
	p := Point{X: 2, Y: 5, SRID: 4171}
	fmt.Println(p.Dimension())
	//Output:0
}

func ExamplePoint_Equals() {
	p := Point{X: 2, Y: 5, SRID: 4171}
	// Equality with Polygon
	g1 := Geometry(new(Polygon))
	fmt.Println(p.Equals(g1))
	// Equality with point from an other Spatial reference
	g2 := Geometry(&Point{X: 2, Y: 5, SRID: 9999})
	fmt.Println(p.Equals(g2))
	// Equality with 3D Point
	g3 := Geometry(&Point{X: 2, Y: 5, Z: 9999, SRID: 4171})
	fmt.Println(p.Equals(g3))
	// Equality ...
	g4 := Geometry(&Point{X: 2, Y: 5, Z: 0, M: 0, SRID: 4171})
	fmt.Println(p.Equals(g4))
	//Output:
	//false
	//false
	//false
	//true
}

func ExamplePoint_GeometryType() {
	p := Point{X: 2, Y: 5, SRID: 4171}
	fmt.Println(p.GeometryType())
	//Output:POINT
}

func ExamplePoint_Is3D() {
	// 2D point
	p1 := Point{X: 2, Y: 5, SRID: 4171}
	fmt.Println(p1.Is3D())
	// 2D measured point
	p2 := Point{X: 2, Y: 5, M: 789, SRID: 4171}
	fmt.Println(p2.Is3D())
	// 3D point
	p3 := Point{X: 2, Y: 5, Z: 4807}
	fmt.Println(p3.Is3D())
	//Output:
	//false
	//false
	//true
}

func ExamplePoint_IsEmpty() {
	// Empty point
	p1 := Point{}
	fmt.Println(p1.IsEmpty())
	// Not empty point
	p2 := Point{SRID: 4171}
	fmt.Println(p2.IsEmpty())
	//Output:
	//true
	//false
}

func ExamplePoint_IsMeasured() {
	// 2D point
	p1 := Point{X: 2, Y: 5, SRID: 4171}
	fmt.Println(p1.IsMeasured())
	// 2D measured point
	p2 := Point{X: 2, Y: 5, M: 789, SRID: 4171}
	fmt.Println(p2.IsMeasured())
	// 3D point
	p3 := Point{X: 2, Y: 5, Z: 4807}
	fmt.Println(p3.IsMeasured())
	//Output:
	//false
	//true
	//false
}

func ExamplePoint_ToWKT() {
	// Empty point
	p1 := Point{}
	fmt.Println(p1.ToWKT())
	// 2D point
	p2 := Point{X: 12.34, Y: 56.78}
	fmt.Println(p2.ToWKT())
	// 3D point
	p3 := Point{X: 12.34, Y: 56.78, Z: -12}
	fmt.Println(p3.ToWKT())
	// 2D measured point
	p4 := Point{X: 12.34, Y: 56.78, M: 654321}
	fmt.Println(p4.ToWKT())
	// 3D measured point
	p5 := Point{X: 12.34, Y: 56.78, Z: 654321, M: 321}
	fmt.Println(p5.ToWKT())
	//Output:
	//POINT EMPTY
	//POINT (12.34 56.78)
	//POINT Z (12.34 56.78 -12)
	//POINT M (12.34 56.78 654321)
	//POINT ZM (12.34 56.78 654321 321)
}
