/*
example_linestring_test.go is part of Cassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Module "ogeo.gis.geometry.example_linestring_test.go" contains examples use
 to documents LineString class in this package.
*/
package geometry

import (
	"fmt"
)

// -----------------------------------------------------------------------------
// Geometry interface implementation example
// -----------------------------------------------------------------------------

func ExampleLineString_AsText() {
	// Empty linestring
	le := LineString{}
	fmt.Println(le.AsText())
	// LineString with 2D point
	l := LineString{
		Points: []Point{
			Point{X: 2, Y: 5, SRID: 4171},
			Point{X: 98, Y: 45, SRID: 4171},
			Point{X: 21, Y: 52, SRID: 4171},
		},
	}
	fmt.Println(l.AsText())
	// 3D point
	lz := LineString{
		Points: []Point{
			Point{X: 2, Y: 5, Z: 4807, SRID: 4171},
			Point{X: 98, Y: 45, SRID: 4171},
			Point{X: 21, Y: 52, Z: 1021, SRID: 4171},
		},
	}
	fmt.Println(lz.AsText())
	//Output:
	//LINESTRING EMPTY
	//LINESTRING (2 5, 98 45, 21 52)
	//LINESTRING Z (2 5 4807, 98 45 0, 21 52 1021)
}

func ExampleLineString_Dimension() {
	l := LineString{
		Points: []Point{
			Point{X: 2, Y: 5, SRID: 4171},
			Point{X: 98, Y: 45, SRID: 4171},
			Point{X: 21, Y: 52, SRID: 4171},
		},
	}
	fmt.Println(l.Dimension())
	//Output:1
}

func ExampleLineString_Equals() {
	l := LineString{
		SRID: 4171,
		Points: []Point{
			Point{X: 2, Y: 5, SRID: 4171},
			Point{X: 98, Y: 45, SRID: 4171},
			Point{X: 21, Y: 52, SRID: 4171},
		},
	}
	// Equality with Point
	g1 := Geometry(new(Point))
	fmt.Println(l.Equals(g1))
	// Equality with linestring from an other Spatial reference
	g2 := Geometry(
		&LineString{
			SRID: 9999,
			Points: []Point{
				Point{X: 2, Y: 5},
				Point{X: 98, Y: 45},
				Point{X: 21, Y: 52}}})
	fmt.Println(l.Equals(g2))
	// Equality ...
	g3 := Geometry(&LineString{
		SRID: 4171,
		Points: []Point{
			Point{X: 2, Y: 5, SRID: 4171},
			Point{X: 98, Y: 45, SRID: 4171},
			Point{X: 21, Y: 52, SRID: 4171},
		},
	})
	fmt.Println(l.Equals(g3))
	//Output:
	//false
	//false
	//true
}

func ExampleLineString_GeometryType() {
	l := LineString{
		SRID: 4171,
		Points: []Point{
			Point{X: 2, Y: 5, SRID: 4171},
			Point{X: 98, Y: 45, SRID: 4171},
			Point{X: 21, Y: 52, SRID: 4171},
		},
	}
	fmt.Println(l.GeometryType())
	//Output:LINESTRING
}

func ExampleLineString_Is3D() {
	// Linestring with only 2D point
	l1 := LineString{
		SRID: 4171,
		Points: []Point{
			Point{X: 2, Y: 5, SRID: 4171},
			Point{X: 98, Y: 45, SRID: 4171},
			Point{X: 21, Y: 52, SRID: 4171},
		},
	}
	fmt.Println(l1.Is3D())
	// LineString with one or more 3D point
	l2 := LineString{
		SRID: 4171,
		Points: []Point{
			Point{X: 2, Y: 5, Z: 4807, SRID: 4171},
			Point{X: 98, Y: 45, SRID: 4171},
			Point{X: 21, Y: 52, SRID: 4171},
		},
	}
	fmt.Println(l2.Is3D())
	//Output:
	//false
	//true
}

func ExampleLineString_IsEmpty() {
	// Empty lineString
	l1 := LineString{}
	fmt.Println(l1.IsEmpty())
	// Not empty point
	l2 := LineString{Points: []Point{Point{SRID: 4171}}}
	fmt.Println(l2.IsEmpty())
	//Output:
	//true
	//false
}

func ExampleLineString_IsMeasured() {
	// Linestring with only 2D point
	l1 := LineString{
		SRID: 4171,
		Points: []Point{
			Point{X: 2, Y: 5, SRID: 4171},
			Point{X: 98, Y: 45, SRID: 4171},
			Point{X: 21, Y: 52, SRID: 4171},
		},
	}
	fmt.Println(l1.IsMeasured())
	// LineString with one or more measured point
	l2 := LineString{
		SRID: 4171,
		Points: []Point{
			Point{X: 2, Y: 5, M: 4807, SRID: 4171},
			Point{X: 98, Y: 45, SRID: 4171},
			Point{X: 21, Y: 52, SRID: 4171},
		},
	}
	fmt.Println(l2.IsMeasured())
	//Output:
	//false
	//true
}

func ExampleLineString_ToWKT() {
	// Empty linestring
	le := LineString{}
	fmt.Println(le.AsText())
	// LineString with 2D point
	l := LineString{
		Points: []Point{
			Point{X: 2, Y: 5, SRID: 4171},
			Point{X: 98, Y: 45, SRID: 4171},
			Point{X: 21, Y: 52, SRID: 4171},
		},
	}
	fmt.Println(l.AsText())
	// 3D point
	lz := LineString{
		Points: []Point{
			Point{X: 2, Y: 5, Z: 4807, SRID: 4171},
			Point{X: 98, Y: 45, SRID: 4171},
			Point{X: 21, Y: 52, Z: 1021, SRID: 4171},
		},
	}
	fmt.Println(lz.AsText())
	//Output:
	//LINESTRING EMPTY
	//LINESTRING (2 5, 98 45, 21 52)
	//LINESTRING Z (2 5 4807, 98 45 0, 21 52 1021)
}

// -----------------------------------------------------------------------------
// Publics methods tests
// -----------------------------------------------------------------------------

func ExampleLineString_Append() {
	l := &LineString{Points: []Point{
		Point{X: 1, Y: 2},
		Point{X: 3, Y: 4},
		Point{X: 5, Y: 6, Z: 7, SRID: 8}}}
	p := Point{X: 999, Y: 888}
	l.Append(p)
	fmt.Println(l.AsText())
	//Output:LINESTRING Z (1 2 0, 3 4 0, 5 6 7, 999 888 0)
}

func ExampleLineString_EndPoint() {
	l := &LineString{Points: []Point{
		Point{X: 1, Y: 2},
		Point{X: 3, Y: 4},
		Point{X: 5, Y: 6, Z: 7, SRID: 8}}}
	p, err := l.EndPoint()
	if err != nil {
		// Here manage error !
	} else {
		fmt.Println(p.AsText())
	}
	//Output:POINT Z (5 6 7)
}

func ExampleLineString_Insert() {
	l := &LineString{Points: []Point{
		Point{X: 1, Y: 2},
		Point{X: 3, Y: 4},
		Point{X: 5, Y: 6, Z: 7, SRID: 8}}}
	p := Point{X: 999, Y: 888}
	// Normal insertion
	err := l.Insert(p, 2)
	if err != nil {
		// Here manage error !
	} else {
		fmt.Println(l.AsText())
	}
	//Output:LINESTRING Z (1 2 0, 3 4 0, 999 888 0, 5 6 7)
}

func ExampleLineString_Length() {
	l := &LineString{Points: []Point{
		Point{X: 1, Y: 2},
		Point{X: 3, Y: 4},
		Point{X: 5, Y: 6, M: 7, SRID: 9},
		Point{X: 5, Y: 2},
		Point{X: 1, Y: 2}}}
	fmt.Println(l.Length())
	//Output:13.65685424949238
}

func ExampleLineString_NumPoints() {
	l := &LineString{Points: []Point{
		Point{X: 1, Y: 2},
		Point{X: 3, Y: 4},
		Point{X: 5, Y: 6, M: 7, SRID: 9},
		Point{X: 5, Y: 2},
		Point{X: 1, Y: 2}}}
	fmt.Println(l.NumPoints())
	//Output:5
}

func ExampleLineString_PointN() {
	l := LineString{Points: []Point{
		Point{X: 5, Y: 6, M: 7, SRID: 8},
		Point{X: 3, Y: 4},
		Point{X: 5, Y: 6},
		Point{X: 7, Y: 8},
		Point{X: 1, Y: 2},
		Point{X: 5, Y: 6, M: 7, SRID: 8}}}
	p, err := l.PointN(3)
	if err != nil {
		// Here manage error !
	} else {
		fmt.Println(p.AsText())
	}
	//Output:POINT (7 8)
}

func ExampleLineString_StartPoint() {
	l := &LineString{Points: []Point{
		Point{X: 1, Y: 2},
		Point{X: 3, Y: 4},
		Point{X: 5, Y: 6, Z: 7, SRID: 8}}}
	p, err := l.StartPoint()
	if err != nil {
		fmt.Println("Manage error !")
	} else {
		fmt.Println(p.AsText())
	}
	//Output:POINT (1 2)
}
