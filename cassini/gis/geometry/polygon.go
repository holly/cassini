/*
polygon.go is part of Cassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Module "ogeo.gis.geometry.polygon.go" contains code to implements Geometry
interface for geometry sub-class "Polygon", code to export Polygon in some
file format and code to create Polygon (import) with some file format.
*/
package geometry

import (
	"fmt"
)

/*
Polygon is a planar Surface defined by 1 exterior boundary and 0 or more
interior boundaries. Each interior boundary defines a hole in the Polygon.

Surface

A Surface is a 2-dimensional geometric object.

A simple Surface may consists of a single “patch” that is associated with one
“exterior boundary” and 0 or more “interior” boundaries. A single such Surface
patch in 3-dimensional space is isometric to planar Surfaces, by a simple affine
rotation matrix that rotates the patch onto the plane z = 0. If the patch is not
vertical, the projection onto the same plane is an isomorphism, and can be
represented as a linear transformation, i.e. an affine.

Polyhedral Surfaces are formed by “stitching” together such simple Surfaces
patches along their common boundaries. Such polyhedral Surfaces in a
3-dimensional space may not be planar as a whole, depending on the orientation
of their planar normals (Reference [1], sections 3.12.9.1, and 3.12.9.3). If all
the patches are in alignment (their normals are parallel), then the whole
stitched polyhedral surface is co-planar and can be represented as a single
patch if it is connected.

The boundary of a simple Surface is the set of closed Curves corresponding to
its “exterior” and “interior” boundaries (Reference [1], section 3.12.9.4).

The only instantiable subclasses of Surface defined in this standard are Polygon
and PolyhedralSurface. A Polygon is a simple Surface that is planar. A
PolyhedralSurface is a simple surface, consisting of some number of Polygon
patches or facets. If a PolyhedralSurface is closed, then it bounds a solid. A
MultiSurface containing a set of closed PolyhedralSurfaces can be used to
represent a Solid object with holes.

Surface implments following methods :

Area (): Double — The area of this Surface, as measured in the spatial
reference system of this Surface.

⎯ Centroid (): Point — The mathematical centroid for this Surface as a Point.
The result is not guaranteed to be on this Surface.

⎯ PointOnSurface (): Point — A Point guaranteed to be on this Surface.

Polygon

A Polygon is a planar Surface defined by 1 exterior boundary and 0 or more
interior boundaries. Each interior boundary defines a hole in the Polygon.

A Triangle is a polygon with 3 distinct, non-collinear vertices and no
interior boundary.

The exterior boundary LinearRing defines the “top” of the surface which is
the side of the surface from which the exterior boundary appears to traverse
the boundary in a counter clockwise direction. The interior LinearRings will
have the opposite orientation, and appear as clockwise when viewed from the
“top”,

The assertions for Polygons (the rules that define valid Polygons) are as
follows:

a) Polygons are topologically closed ;

b) The boundary of a Polygon consists of a set of LinearRings that make up
its exterior and interior boundaries ;

c) No two Rings in the boundary cross and the Rings in the boundary of a
Polygon may intersect at a Point but only as a tangent, e.g.

d) A Polygon may not have cut lines, spikes or punctures e.g.

e) The interior of every Polygon is a connected point set;

f) The exterior of a Polygon with 1 or more holes is not connected. Each hole
defines a connected component of the exterior.

Polygon implements folloowing methods :

⎯ ExteriorRing (): LineString — Returns the exterior ring of this Polygon.

⎯ NumInteriorRing (): Integer — Returns the number of interior rings in this
Polygon.

⎯ InteriorRingN (N: Integer): LineString — Returns the N th interior ring for
this Polygon as a LineString.

Example empty polygon :

	polygon := Polygon{}

Example initialized polygon :

	polygon := Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8}
				}
			},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4},
						Point{X: 5, Y: 6, Z: 7, SRID: 8}
					},
				},
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4},
						Point{X: 5, Y: 6, Z: 7, SRID: 8}
					},
				},
			},
			SIRD: 6543}

*/
type Polygon struct {
	SRID          int
	ExteriorRing  LineString
	InteriorRings []LineString
}

// -----------------------------------------------------------------------------
// Geometry interface implementation
// -----------------------------------------------------------------------------

/*
AsText exports this Polygon to a specific Well-knownText Representation
of this Polygon (WKT). It returns a string.
*/
func (p *Polygon) AsText() string {
	return p.ToWKT()
}

/*
Dimension gives the inherent dimension of this Polygon, which must be less
than or equal to the coordinate dimension. A Polygon is a 2-dimensional
geometric object like Surface.
*/
func (p *Polygon) Dimension() int {
	return 2
}

/*
Equals(anotherGeometry: Geometry) returns true if this other geometry is
Polygon type, having the same spatial reference, identical exterior ring, and
identical interior rings. Notes that interior ring are not sequenced. Say
geometric object is “spatially equal” to anotherGeometry.
*/
func (p *Polygon) Equals(g Geometry) bool {
	pB, ok := (g.(*Polygon))
	if !ok {
		return false
	}
	if p.SRID != pB.SRID {
		return false
	}
	if !p.ExteriorRing.Equals(&pB.ExteriorRing) {
		return false
	}
	// Interior ring should equals but can be not ordered ...
	for i, _ := range p.InteriorRings {
		match := false
		ls := p.InteriorRings[i]
		for _, innerLs := range pB.InteriorRings {
			if ls.Equals(&innerLs) {
				match = true
			}
		}
		if !match {
			return false
		}
	}
	return true
}

/*
GeometryType returns the name of the instantiable subtype of Geometry of which
this geometric object is an instantiable member. The name of the subtype of
Geometry is returned as a string.
*/
func (p *Polygon) GeometryType() string {
	return "POLYGON"
}

/*
Is3D returns true if this Polygon have 3D exterior ring or one or more 3D
interior ring.
*/
func (p *Polygon) Is3D() bool {
	if p.ExteriorRing.Is3D() {
		return true
	}
	for _, ls := range p.InteriorRings {
		if ls.Is3D() {
			return true
		}
	}
	return false
}

/*
IsEmpty returns true if this Polygon have empty exterior ring.
*/
func (p *Polygon) IsEmpty() bool {
	if p.ExteriorRing.IsEmpty() {
		return true
	}
	return false
}

/*
IsMeasured returns true if this Polygon have measured exterior ring or one or
more measured interior ring.
*/
func (p *Polygon) IsMeasured() bool {
	if p.ExteriorRing.IsMeasured() {
		return true
	}
	for _, ls := range p.InteriorRings {
		if ls.IsMeasured() {
			return true
		}
	}
	return false
}

/*
ToWKT exports this Polygon to a specific Well-knownText Representation
of this Polygon (WKT). It returns a string.

Example 3D Polygon

	pRef := &Polygon{
			ExteriorRing: LineString{
				Points: []Point{
					Point{X: 1, Y: 2},
					Point{X: 3, Y: 4},
					Point{X: 5, Y: 6, Z: 7, SRID: 8}}},
			InteriorRings: []LineString{
				LineString{
					Points: []Point{
						Point{X: 1, Y: 2},
						Point{X: 3, Y: 4},
						Point{X: 5, Y: 6, SRID: 8}}}}}
	fmt.Println(pRef.ToWKT())

returns

	POLYGON Z ((1 2 0, 3 4 0, 5 6 7), (1 2 0, 3 4 0, 5 6 0))

*/
func (p *Polygon) ToWKT() string {
	if p.IsEmpty() {
		return fmt.Sprintf("%s EMPTY", p.GeometryType())
	}
	s := p.subType()
	switch s {
	case "":
		return fmt.Sprintf(
			"POLYGON %s",
			p.innerWKT(s))
	case "Z":
		return fmt.Sprintf(
			"POLYGON Z %s",
			p.innerWKT(s))
	case "M":
		return fmt.Sprintf(
			"POLYGON M %s",
			p.innerWKT(s))
	case "ZM":
		return fmt.Sprintf(
			"POLYGON ZM %s",
			p.innerWKT(s))
	}
	return ""
}

// -----------------------------------------------------------------------------
// Publics methods
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Privates methods
// -----------------------------------------------------------------------------
func (p *Polygon) subType() string {
	k := ""
	if p.Is3D() {
		k = "Z"
	}
	if p.IsMeasured() {
		k += "M"
	}
	return k
}

func (p *Polygon) innerWKT(s string) string {
	wkt := "("
	wkt += p.ExteriorRing.innerWKT(s)
	for _, ls := range p.InteriorRings {
		wkt += ", "
		wkt += ls.innerWKT(s)
	}
	wkt += ")"
	return wkt
}
