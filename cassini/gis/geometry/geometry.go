/*
geometry.go is part of Cassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Module "ogeo.gis.geometry.geometry.go" contains all interfaces use by
geometries features.
*/
package geometry

// SEE --> doc.go
// FIX ME --> complete interface implementation

const (
	epsilon float64 = 0.0000000001
)

/*
Geometry interface define common geometry methods.
Details is explained into package documentation.
*/
type Geometry interface {
	// OpenGis basics methods
	AsText() string
	//AsBinary() []byte
	//Boundary() Geometry
	Dimension() int
	//Envelope() (Polygon, error)
	IsEmpty() bool
	//IsSimple() bool
	Is3D() bool
	IsMeasured() bool
	GeometryType() string
	// OpenGis testing methods
	//Contains(anotherGeometry Geometry) bool
	//Crosses(anotherGeometry Geometry) bool
	//Disjoint(anotherGeometry Geometry) bool
	Equals(anotherGeometry Geometry) bool
	//Intersects(anotherGeometry Geometry) bool
	//LocateAlong(mValue float64) Geometry
	//LocateBetween(mStart float64, mEnd: float) Geometry
	//Overlaps(anotherGeometry Geometry) bool
	//Relate(anotherGeometry Geometry, intersectionPatternMatrix: string) bool
	//Touches(anotherGeometry Geometry) bool
	//Within(anotherGeometry Geometry) bool
	// OpenGis spatial analysis methods
	// Cassini methods
	ToWKT() string
}
