/*
point_test.go is part of Cassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package geometry

import "testing"

// Test Geometry interface implementation
func testPointImplementGeometry(t *testing.T) {
	var _ Geometry = (*Point)(nil)
}

func testPointDimension(t *testing.T) {
	pRef := &Point{}
	if pRef.Dimension() != 0 {
		t.Errorf(
			"Point.Dimension() fail ! Result %s --> expected 0",
			pRef.Dimension())
	}
}

func TestPointEquals(t *testing.T) {
	testCases := []struct {
		n        int
		pRef     *Point
		g        Geometry
		expected bool
	}{
		{1, &Point{}, &Point{}, true},
		{2, &Point{X: 12.34, Y: 56.78}, &Point{}, false},
		{3, &Point{}, &Point{X: 12.34, Y: 56.78, Z: -12}, false},
		{4, &Point{X: 12.34, Y: 56.78, Z: 654321}, &Point{X: 12.34, Y: 56.78, Z: 654321}, true},
		{5, &Point{X: 12.34, Y: 56.78, Z: 654321}, &Polygon{}, false},
	}
	for _, tc := range testCases {
		p := tc.pRef
		if p.Equals(tc.g) != tc.expected {
			t.Errorf("Point.Equals(Point) test %d fail ! Result %s --> expected %s",
				tc.n,
				p.AsText(),
				tc.g.AsText())
		}
	}
}

func testPointGeometryType(t *testing.T) {
	pRef := &Point{}
	if pRef.GeometryType() != "POINT" {
		t.Errorf(
			"Point.GeometryType() fail ! Result %s --> expected POINT",
			pRef.GeometryType())
	}
}

func TestPointIs3D(t *testing.T) {
	testCases := []struct {
		n        int
		pRef     *Point
		expected bool
	}{
		{1, &Point{}, false},
		{2, &Point{X: 12.34, Y: 56.78}, false},
		{3, &Point{X: 12.34, Y: 56.78, Z: -12}, true},
		{4, &Point{X: 12.34, Y: 56.78, Z: 654321}, true},
	}
	for _, tc := range testCases {
		if tc.pRef.Is3D() != tc.expected {
			t.Errorf(
				"Point.Is3D() test %d fail ! Result %s --> expected %s '",
				tc.n,
				tc.pRef.Is3D(),
				tc.expected)
		}
	}
}

func TestPointIsEmpty(t *testing.T) {
	testCases := []struct {
		n        int
		pRef     *Point
		expected bool
	}{
		{1, &Point{}, true},
		{2, &Point{X: 1}, false},
		{3, &Point{Z: 654321}, false},
	}
	for _, tc := range testCases {
		if tc.pRef.IsEmpty() != tc.expected {
			t.Errorf(
				"Point.IsEmpty() test %d fail ! Result %s --> expected %s",
				tc.n,
				tc.pRef.IsEmpty(),
				tc.expected)
		}
	}
}

func TestPointIsMeasured(t *testing.T) {
	testCases := []struct {
		n        int
		pRef     *Point
		expected bool
	}{
		{1, &Point{}, false},
		{2, &Point{X: 1}, false},
		{3, &Point{M: 654321}, true},
	}
	for _, tc := range testCases {
		if tc.pRef.IsMeasured() != tc.expected {
			t.Errorf("Point.IsMeasured() test %d fail ! Result %s --> expected %s",
				tc.n,
				tc.pRef.IsMeasured(),
				tc.expected)
		}
	}
}

func TestPointToWKT(t *testing.T) {
	testCases := []struct {
		n        int
		pRef     *Point
		expected string
	}{
		{1, &Point{}, "POINT EMPTY"},
		{2, &Point{X: 12.34, Y: 56.78}, "POINT (12.34 56.78)"},
		{3, &Point{X: 12.34, Y: 56.78, Z: -12}, "POINT Z (12.34 56.78 -12)"},
		{4, &Point{X: 12.34, Y: 56.78, Z: 654321}, "POINT Z (12.34 56.78 654321)"},
		{5, &Point{X: 12.34, Y: 56.78, M: 654}, "POINT M (12.34 56.78 654)"},
		{6, &Point{X: 12.34, Y: 56.78, Z: 654321, M: 321}, "POINT ZM (12.34 56.78 654321 321)"},
	}
	for _, tc := range testCases {
		if tc.pRef.ToWKT() != tc.expected {
			t.Errorf("Point.ToWKT() test %d fail ! Result %s --> expected %s",
				tc.n,
				tc.pRef.ToWKT(),
				tc.expected)
		}
	}
}
