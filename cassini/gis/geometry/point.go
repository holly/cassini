/*

point.go is part of Cassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Module "ogeo.gis.geometry.point.go" contains code to implements Geometry
interface for geometry sub-class "Point", code to export Point in some file
format and code to create Point (import) with some file format.
*/
package geometry

import (
	"fmt"
)

/*
Point is a 0-dimensional geometric object and represents a single location
in coordinate space. A Point has an x-coordinate value, a y-coordinate value.
If called for by the associated Spatial Reference System, it may also have
coordinate values for z-coordinate (elevation) and m-coordinate (measurement).

SRID is the Spatial Reference System ID for this geometric object. This
will normally be a foreign key to an index of reference systems stored in either
the same or some other datastore.

NOTE : Parameters default values are 0

*/
type Point struct {
	X    float64
	Y    float64
	Z    float64
	M    float64
	SRID int
}

// -----------------------------------------------------------------------------
// Geometry interface implementation
// -----------------------------------------------------------------------------

/*
AsText exports this Point to a specific Well-knownText Representation
of this Point (WKT). It returns a string.
*/
func (p *Point) AsText() string {
	return p.ToWKT()
}

/*
Dimension gives the inherent dimension of this Point, which must be less than or
equal to the coordinate dimension. A Point is a 0-dimensional geometric object.
It returns constant 0.
*/
func (p *Point) Dimension() int {
	return 0
}

/*
Equals(anotherGeometry: Geometry) returns true if this other geometry is
Point type and having equality to coordinates, elevation and measure into the
the same spatial réference. Say geometric object is “spatially equal” to
anotherGeometry.
*/
func (p *Point) Equals(g Geometry) bool {
	pB, ok := (g.(*Point))
	if !ok {
		return false
	}
	if p.X != pB.X || p.Y != pB.Y || p.Z != pB.Z ||
		p.M != pB.M || p.SRID != pB.SRID {
		return false
	}
	return true
}

/*
Envelope() returns the minimum bounding box for this Geometry, returned
Polygon and error. Because Point have only one dimension, this method returns
constant empty Polygon and constant error !
*/

/*func (p *Point) Envelope() (Polygon, error) {
	return Polygon{}, errors.New(
		"Error from [cassini.gis.geometry.Point.envelope] : " +
			"Point can not having envelope.")
}*/

/*
GeometryType returns the name of the instantiable subtype of Geometry of which
this geometric object is an instantiable member. The name of the subtype of
Geometry is returned as a string.
*/
func (p *Point) GeometryType() string {
	return "POINT"
}

/*
Is3D returns true if this Point have Z coordinate values not equals to zero.
*/
func (p *Point) Is3D() bool {
	if p.Z != 0 {
		return true
	}
	return false
}

/*
IsEmpty returns true if this Point is empty, say X=Y=Z=M=SRID=0.
*/
func (p *Point) IsEmpty() bool {
	if p.X == 0 && p.Y == 0 && p.Z == 0 && p.M == 0 && p.SRID == 0 {
		return true
	}
	return false
}

/*
IsMeasured returns true if this Point have M coordinate values.
*/
func (p *Point) IsMeasured() bool {
	if p.M != 0 {
		return true
	}
	return false
}

/*
ToWKT exports this Point to a specific Well-knownText Representation
of this Point (WKT). It returns a string.
*/
func (p *Point) ToWKT() string {
	if p.IsEmpty() {
		return fmt.Sprint("POINT EMPTY")
	}
	s := p.subType()
	switch s {
	case "":
		return fmt.Sprintf(
			"POINT (%s)",
			p.innerWKT(s))
	case "Z":
		return fmt.Sprintf(
			"POINT Z (%s)",
			p.innerWKT(s))
	case "M":
		return fmt.Sprintf(
			"POINT M (%s)",
			p.innerWKT(s))
	case "ZM":
		return fmt.Sprintf(
			"POINT ZM (%s)",
			p.innerWKT(s))
	}
	return ""
}

// -----------------------------------------------------------------------------
// Publics methods
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Privates methods
// -----------------------------------------------------------------------------

func (p *Point) subType() string {
	var k string
	if p.Is3D() {
		k = "Z"
	}
	if p.IsMeasured() {
		k += "M"
	}
	return k
}

func (p *Point) innerWKT(s string) string {
	switch s {
	case "":
		return fmt.Sprintf("%g %g", p.X, p.Y)
	case "Z":
		return fmt.Sprintf("%g %g %g", p.X, p.Y, p.Z)
	case "M":
		return fmt.Sprintf("%g %g %g", p.X, p.Y, p.M)
	case "ZM":
		return fmt.Sprintf("%g %g %g %g", p.X, p.Y, p.Z, p.M)
	}
	return ""
}
