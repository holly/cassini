/*
core.go is part of Cassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Module "ogeo.gis.srs.core.go" contains code to implements srs model object :
structure and methods.
*/
package srs

import (
	"fmt"
)

// -----------------------------------------------------------------------------
// Area object
// -----------------------------------------------------------------------------

/*
Area defines a bounding box where something (Datum, Coordinate system, ...)
is valid.
*/
type Area struct {
	SRID              int
	Name              string
	SouthBound        float64
	NorthBound        float64
	WestBound         float64
	EastBound         float64
	CountryIsoAlpha2  string
	CountryIsoAlpha3  string
	CountryIsoNumeric int
	Deprecated        bool
}

// -----------------------------------------------------------------------------
// CoordinateAxis object
// -----------------------------------------------------------------------------

/*
Coordinate axis defines axis use in relation with reference system.
*/
type CoordinateAxis struct {
	SRID             int
	CoordinateSystem CoordinateSystem
	Name             string
	Orientation      string
	Abbreviation     string
	Unit             Unit
	Order            int
}

// -----------------------------------------------------------------------------
// CoordinateOperation object
// -----------------------------------------------------------------------------

/*
CoordinateOperation defines operation to transform or convert to other CRS.
*/
type CoordinateOperation struct {
	SRID          int
	Name          string
	Type          string
	CrsSourceSRID int
	CrsTargetSRID int
	AreaOfUse     Area
	Scope         string
	Accuracy      float64
	Method        CoordinateOperationMethod
	Deprecated    bool
}

// -----------------------------------------------------------------------------
// CoordinateOperationMethod object
// -----------------------------------------------------------------------------

/*
CoordinateOperationMethod defines method use in operation to transform or
conversion to other CRS.
*/
type CoordinateOperationMethod struct {
	SRID       int
	Name       string
	Reverse    bool
	Deprecated bool
}

// -----------------------------------------------------------------------------
// CoordinateOperationParameter object
// -----------------------------------------------------------------------------

/*
CoordinateOperationParameter defines parameter use in operation to transform or
conversion to other CRS.
*/
type CoordinateOperationParameter struct {
	SRID       int
	Name       string
	Deprecated bool
}

// -----------------------------------------------------------------------------
// CoordinateOperationParameterUsage object
// -----------------------------------------------------------------------------

/*
CoordinateOperationParameterUsage defines parameter usage in operation to
transform and convert to other CRS.
Note coumpound primary key ...
*/
type CoordinateOperationParameterUsage struct {
	Method    CoordinateOperationMethod
	Parameter CoordinateOperationParameter
	Order     int
}

// -----------------------------------------------------------------------------
// CoordinateOperationParameterValue object
// -----------------------------------------------------------------------------

/*
CoordinateOperationParameterValue defines parameter value needs in
transformation and convertion to other CRS.
Note coumpound primary key ...
*/
type CoordinateOperationParameterValue struct {
	Operation CoordinateOperation
	Method    CoordinateOperationMethod
	Parameter CoordinateOperationParameter
	Value     float64
	Unit      Unit
}

// -----------------------------------------------------------------------------
// CoordinateReferenceSystem object
// -----------------------------------------------------------------------------

/*
CoordinateReferenceSystem (CRS) consist of one coordinate system that is related to an object
through one datum. For the CRSs of interest to the EPSG Dataset, that object is the Earth.

A coordinate system (CS) is a sequence of coordinate axes with specified units of measure. A coordinate
system is an abstract mathematical concept without any defined relationship to the Earth. Coordinate
systems generally have not been explicitly described in geodetic literature, and they rarely have well-
known names by which they are identified. The historic colloquial use of 'coordinate system' usually
meant coordinate reference system.

A datum specifies the relationship of a coordinate system to the Earth, thus ensuring that the abstract
mathematical concept can be applied to the practical problem of describing positions of features on or near
the earth’s surface by means of coordinates.

Coordinate reference systems, coordinate systems and datums are each classified into several subtypes.
Each coordinate system type can be associated with only specific types of coordinate reference system.
Similarly each datum type can be associated with only specific types of coordinate reference system. Thus,
indirectly through their association with CRS types, each coordinate system type can only be associated
with specific types of datum.
*/
type CoordinateReferenceSystem struct {
	SRID                 int
	Name                 string
	AreaOfUse            Area
	Type                 string
	CoordinateSystem     CoordinateSystem
	Datum                Datum
	GeographicCrsSource  int
	ProjectionConversion int
	Deprecated           bool
}

// -----------------------------------------------------------------------------
// CoordinateSystem object
// -----------------------------------------------------------------------------

/*
CoordinateSystem defines a coordinate system uses by coordinate reference system
and Coordinate axis.
*/
type CoordinateSystem struct {
	SRID       int
	Name       string
	Type       string
	Dimension  int
	Deprecated bool
}

// -----------------------------------------------------------------------------
// Datum object
// -----------------------------------------------------------------------------

/*
Datum defines the relationship of a geographic or geocentric coordinate system
to the earth.
*/
type Datum struct {
	SRID          int
	Name          string
	Type          string
	Ellipsoid     Ellipsoid
	PrimeMeridian PrimeMeridian
	AreaOfUse     Area
	Deprecated    bool
}

/*
ToWKT returns string as Well Know Text representation.
*/
func (d *Datum) ToWKT() string {
	return fmt.Sprintf(
		"DATUM [\"%s\", %s]",
		d.Name,
		d.Ellipsoid.ToWKT())
}

// -----------------------------------------------------------------------------
// Ellipsoid object
// -----------------------------------------------------------------------------

/*
Ellipsoid is simplified by modelling of the Earth. Parameters are required to
describe the size and shape of an oblate ellipsoid : attribute semi-major-axis
and inverseflattening. They are used to compute others ellipsoid's parameters
(Notes that computation is made during database build).
*/
type Ellipsoid struct {
	SRID               int
	Name               string
	SemiMajorAxis      float64
	Unit               Unit
	InvFlattening      float64
	IsSphere           bool
	SemiMinorAxis      float64
	Flattening         float64
	Eccentricity       float64
	SecondEccentricity float64
	Deprecated         bool
}

/*
ToWKT returns string as Well Know Text representation.
*/
func (e *Ellipsoid) ToWKT() string {
	return fmt.Sprintf(
		"ELLIPSOID [\"%s\", %.f, %g]",
		e.Name,
		e.SemiMajorAxis,
		e.InvFlattening)
}

// -----------------------------------------------------------------------------
// PrimeMeridian object
// -----------------------------------------------------------------------------

/*
PrimeMeridian is reference meridian to define a longitude. Attribute
GreenwichLongitude is angle beetwenn e this PrimeMeridian and thr Greenwich
longitude.
Examples :
> 'Greenwich' prime meridian angle to Greenwich is equal to zero radians.
> 'Paris' prime meridian angle to Greenwich is equal to 2.5969213 grades.
*/
type PrimeMeridian struct {
	SRID               int
	Name               string
	GreenwichLongitude float64
	Unit               Unit
	Deprecated         bool
}

/*
ToWKT returns string as Well Know Text representation.
*/
func (pm *PrimeMeridian) ToWKT() string {
	return fmt.Sprintf(
		"PRIMEM [\"%s\", %g]",
		pm.Name,
		pm.GreenwichLongitude)
}

// -----------------------------------------------------------------------------
// Unit object
// -----------------------------------------------------------------------------

//Unit is unit of measure use by spatial referencing objects.
//Attributes FactorB and FactorC are provided to compute ratio needs to convert
//this Unit to ISO standard unit (radian, meter, second, ...).
//Example, Nautical Mile have ratio FactorB/FactorC equals to 1852/1 and shouldto converting to meter,
//this value are provided by FactorB and FactorC.
type Unit struct {
	SRID           int
	Name           string
	Type           string
	TargetUnitSRID int
	FactorB        float64
	FactorC        float64
	RatioToSI      float64
	Deprecated     bool
}

/*
ToWKT returns string as Well Know Text representation.
*/
func (u *Unit) ToWKT() string {
	return fmt.Sprintf("UNIT [\"%s\", %g]", u.Name, u.FactorB)
}
