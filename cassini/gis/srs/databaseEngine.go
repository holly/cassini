/*
databaseEngine.go is part of Cassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Module "ogeo.gis.srs.databaseEngine.go" contains code to implements static
database structures and methods.
*/
package srs

import (
//"bytes"
//"strings"
)

//------------------------------------------------------------------------------
// Select queries
//------------------------------------------------------------------------------

/*
SelectArea returns Area having this SRID.
Returns Area and true when exists.
Returns empty area and false when is unknown.
*/
func SelectArea(SRID int) (Area, bool) {
	for _, a := range AREAS {
		if a.SRID == SRID {
			return a, true
		}
	}
	return Area{}, false
}

/*
SelectCoordinateAxis returns CoordinateAxis having this SRID.
Returns CoordinateAxise and true when exists.
Returns empty CoordinateAxis and false when is unknown.
*/
func SelectCoordinateAxis(SRID int) (CoordinateAxis, bool) {
	for _, ca := range COORDINATE_AXIS {
		if ca.SRID == SRID {
			cs, cs_exists := SelectCoordinateSystem(ca.CoordinateSystem.SRID)
			u, u_exists := SelectUnit(ca.Unit.SRID)
			if cs_exists && u_exists {
				ca.CoordinateSystem = cs
				ca.Unit = u
				return ca, true
			}
		}
	}
	return CoordinateAxis{}, false
}

/*
SelectCoordinateOperation returns CoordinateOperation having this SRID.
Returns CoordinateOperation and true when exists.
Returns empty CoordinateOperation and false when is unknown.
*/
func SelectCoordinateOperation(SRID int) (CoordinateOperation, bool) {
	for _, co := range COORDINATE_OPERATIONS {
		if co.SRID == SRID {
			a, a_exists := SelectArea(co.AreaOfUse.SRID)
			m, m_exists := SelectCoordinateOperationMethod(co.Method.SRID)
			if a_exists && m_exists {
				co.AreaOfUse = a
				co.Method = m
				return co, true
			}
		}
	}
	return CoordinateOperation{}, false

}

/*
SelectCoordinateOperationMethod returns CoordinateOperationMethod having this SRID.
Returns CoordinateOperationMethod and true when exists.
Returns empty CoordinateOperationMethod and false when is unknown.
*/
func SelectCoordinateOperationMethod(SRID int) (CoordinateOperationMethod, bool) {
	for _, com := range COORDINATE_OPERATION_METHODS {
		if com.SRID == SRID {
			return com, true
		}
	}
	return CoordinateOperationMethod{}, false
}

/*
SelectCoordinateOperationParameter returns CoordinateOperationParameter having this SRID.
Returns CoordinateOperationParameter and true when exists.
Returns empty CoordinateOperationParameter and false when is unknown.
*/
func SelectCoordinateOperationParameter(SRID int) (CoordinateOperationParameter, bool) {
	for _, cop := range COORDINATE_OPERATION_PARAMETERS {
		if cop.SRID == SRID {
			return cop, true
		}
	}
	return CoordinateOperationParameter{}, false
}

/*
SelectCoordinateOperationParameterUsage returns slice of
CoordinateOperationParameterUsage having this method SRID and items number in slice.
*/
func SelectCoordinateOperationParameterUsage(MethodSRID int) ([]CoordinateOperationParameterUsage, int) {
	l := []CoordinateOperationParameterUsage{}
	for _, copu := range COORDINATE_OPERATION_PARAMETER_USAGES {
		if copu.Method.SRID == MethodSRID {
			m, m_exists := SelectCoordinateOperationMethod(copu.Method.SRID)
			p, p_exists := SelectCoordinateOperationParameter(copu.Parameter.SRID)
			if m_exists && p_exists {
				copu.Method = m
				copu.Parameter = p
				l = append(l, copu)
			}
		}
	}
	return l, len(l)
}

/*
SelectCoordinateOperationParameterValue returns slice of
CoordinateOperationParameterValue having this operation SRID and items number in slice.
*/
func SelectCoordinateOperationParameterValue(OperationSRID int, MethodSRID int) ([]CoordinateOperationParameterValue, int) {
	l := []CoordinateOperationParameterValue{}
	for _, copv := range COORDINATE_OPERATION_PARAMETER_VALUES {
		if copv.Operation.SRID == OperationSRID && copv.Method.SRID == MethodSRID {
			o, o_exists := SelectCoordinateOperation(copv.Operation.SRID)
			m, m_exists := SelectCoordinateOperationMethod(copv.Method.SRID)
			p, p_exists := SelectCoordinateOperationParameter(copv.Parameter.SRID)
			if o_exists && m_exists && p_exists {
				copv.Operation = o
				copv.Method = m
				copv.Parameter = p
				l = append(l, copv)
			}
		}
	}
	return l, len(l)
}

/*
SelectCoordinateReferenceSystem returns Crs having this SRID.
Returns Crs and true when Crs exists.
Returns empty Crs and false when is unknown.
*/
func SelectCoordinateReferenceSystem(SRID int) (CoordinateReferenceSystem, bool) {
	for _, crs := range COORDINATE_REFERENCE_SYSTEMS {
		if crs.SRID == SRID {
			if crs.CoordinateSystem.SRID > 0 {
				cs, _ := SelectCoordinateSystem(crs.CoordinateSystem.SRID)
				crs.CoordinateSystem = cs
			}
			if crs.Datum.SRID > 0 {
				d, _ := SelectDatum(crs.Datum.SRID)
				crs.Datum = d
			}
			a, a_exists := SelectArea(crs.AreaOfUse.SRID)
			if a_exists {
				crs.AreaOfUse = a
				return crs, true
			}
		}
	}
	return CoordinateReferenceSystem{}, false
}

/*
SelectCoordinateSystem returns coordinateSystem having this SRID.
Returns Coordinatesystem and true when exists.
Returns empty CoordinateSystem and false when is unknown.
*/
func SelectCoordinateSystem(SRID int) (CoordinateSystem, bool) {
	for _, cs := range COORDINATE_SYSTEMS {
		if cs.SRID == SRID {
			return cs, true
		}
	}
	return CoordinateSystem{}, false
}

/*
SelectDatum returns Datum having this SRID.
Returns Datum and true when exists.
Returns empty Datum and false when is unknown.
*/
func SelectDatum(SRID int) (Datum, bool) {
	for _, d := range DATUMS {
		if d.SRID == SRID {
			if d.Ellipsoid.SRID > 0 {
				e, _ := SelectEllipsoid(d.Ellipsoid.SRID)
				d.Ellipsoid = e
			}
			if d.PrimeMeridian.SRID > 0 {
				pm, _ := SelectPrimeMeridian(d.PrimeMeridian.SRID)
				d.PrimeMeridian = pm
			}
			a, a_exists := SelectArea(d.AreaOfUse.SRID)
			if a_exists {
				d.AreaOfUse = a
				return d, true
			}
		}
	}
	return Datum{}, false
}

/*
SelectEllipsoid, returns Ellipsoid having this SRID.
Returns Ellipsoid and true when exists.
Returns empty Ellipsoid and false when is unknown.
*/
func SelectEllipsoid(SRID int) (Ellipsoid, bool) {
	for _, e := range ELLIPSOIDS {
		if e.SRID == SRID {
			u, u_exists := SelectUnit(e.Unit.SRID)
			if u_exists {
				e.Unit = u
				return e, true
			}
		}
	}
	return Ellipsoid{}, false
}

/*
SelectPrimeMeridian returns PrimeMeridian structure ask with SRID parameter.
Return PrimeMeridian and true when exists.
Return empty PrimeMeridian and false when is unknown.
*/
func SelectPrimeMeridian(SRID int) (PrimeMeridian, bool) {
	for _, pm := range PRIME_MERIDIANS {
		if pm.SRID == SRID {
			u, u_exists := SelectUnit(pm.Unit.SRID)
			if u_exists {
				pm.Unit = u
				return pm, true
			}
		}
	}
	return PrimeMeridian{}, false
}

/*
SelectUnit returns Unit having this SRID.
Returns Unit and true when exists.
Returns empty Unit and false when is unknown.
*/
func SelectUnit(SRID int) (Unit, bool) {
	for _, u := range UNITS {
		if u.SRID == SRID {
			return u, true
		}
	}
	return Unit{}, false
}
