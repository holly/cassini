/*
databaseEngine_test.go is part of Cassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package srs

import (
	"math"
	"strings"
	"testing"
)

//------------------------------------------------------------------------------
// Select queries
//------------------------------------------------------------------------------

func TestAreaSelect(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected Area
		exists   bool
	}{
		{1, 99992229999, Area{}, false},
		{2, 0, Area{}, false},
		{3, 1024, Area{SRID: 1024, Name: "Afghanistan", SouthBound: 29.4, NorthBound: 38.48, WestBound: 60.5, EastBound: 74.92, CountryIsoAlpha2: "AF", CountryIsoAlpha3: "AFG", CountryIsoNumeric: 4, Deprecated: false}, true},
		{4, 1025, Area{SRID: 1025, Name: "Albania", SouthBound: 39.63, NorthBound: 42.67, WestBound: 18.46, EastBound: 21.06, CountryIsoAlpha2: "AL", CountryIsoAlpha3: "ALB", CountryIsoNumeric: 8, Deprecated: false}, true},
	}
	for _, tc := range testCases {
		a, exists := SelectArea(tc.id)
		if exists != tc.exists {
			t.Errorf("SelectArea() test %d fail ! Must returns not exists.", tc.n)
		}
		if exists {
			if a.SRID != tc.id {
				t.Errorf("SelecArea() test %d fail ! Data SRID corrupted", tc.n)
			}
			if a.Name != tc.expected.Name {
				t.Errorf("SelecArea() test %d fail ! Data Name corrupted", tc.n)
			}
			if a.SouthBound != tc.expected.SouthBound {
				t.Errorf("SelecArea() test %d fail ! Data SouthBound corrupted", tc.n)
			}
			if a.NorthBound != tc.expected.NorthBound {
				t.Errorf("SelecArea() test %d fail ! Data NorthBound corrupted", tc.n)
			}
			if a.WestBound != tc.expected.WestBound {
				t.Errorf("SelecArea() test %d fail ! Data WestBound corrupted", tc.n)
			}
			if a.EastBound != tc.expected.EastBound {
				t.Errorf("SelecArea() test %d fail ! Data EastBound corrupted", tc.n)
			}
			if a.CountryIsoAlpha2 != tc.expected.CountryIsoAlpha2 {
				t.Errorf("SelecArea() test %d fail ! Data CountryIsoAlpha2 corrupted", tc.n)
			}
			if a.CountryIsoAlpha3 != tc.expected.CountryIsoAlpha3 {
				t.Errorf("SelecArea() test %d fail ! Data CountryIsoAlpha3 corrupted", tc.n)
			}
			if a.CountryIsoNumeric != tc.expected.CountryIsoNumeric {
				t.Errorf("SelecArea() test %d fail ! Data CountryIsoNumeric corrupted", tc.n)
			}
			if a.Deprecated != tc.expected.Deprecated {
				t.Errorf("SelecArea() test %d fail ! Data Deprecated corrupted", tc.n)
			}
		}
	}
}

func Test_SelectCoordinateAxis(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected CoordinateAxis
		exists   bool
	}{
		{1, 99992229999, CoordinateAxis{}, false},
		{2, 0, CoordinateAxis{}, false},
		{3, 1039, CoordinateAxis{SRID: 1039, CoordinateSystem: CoordinateSystem{SRID: 1024}, Name: "Easting", Orientation: "east", Abbreviation: "M", Unit: Unit{SRID: 9001}, Order: 1}, true},
		{4, 1040, CoordinateAxis{SRID: 1040, CoordinateSystem: CoordinateSystem{SRID: 1024}, Name: "Northing", Orientation: "north", Abbreviation: "P", Unit: Unit{SRID: 9001}, Order: 2}, true},
	}
	for _, tc := range testCases {
		cs, exists := SelectCoordinateAxis(tc.id)
		if exists != tc.exists {
			t.Errorf("SelectCoordinateAxis() test %d fail ! Must returns don't exists.", tc.n)
		}
		if exists {
			if cs.SRID != tc.id {
				t.Errorf("SelectCoordinateAxis() test %d fail ! Data SRID corrupted", tc.n)
			}
			if cs.CoordinateSystem.SRID != tc.expected.CoordinateSystem.SRID {
				t.Errorf("SelectCoordinateAxis() test %d fail ! Data CoordinateSystem corrupted", tc.n)
			}
			if cs.Name != tc.expected.Name {
				t.Errorf("SelectCoordinateAxis() test %d fail ! Data Name corrupted", tc.n)
			}
			if cs.Orientation != tc.expected.Orientation {
				t.Errorf("SelectCoordinateAxis() test %d fail ! Data Orientation corrupted", tc.n)
			}
			if cs.Abbreviation != tc.expected.Abbreviation {
				t.Errorf("SelectCoordinateAxis() test %d fail ! Data Abbreviation corrupted", tc.n)
			}
			if cs.Unit.SRID != tc.expected.Unit.SRID {
				t.Errorf("SelectCoordinateAxis() test %d fail ! Data Unit corrupted", tc.n)
			}
			if cs.Order != tc.expected.Order {
				t.Errorf("SelectCoordinateAxis() test %d fail ! Data Order corrupted", tc.n)
			}
		}
	}
}

func Test_SelectCoordinateSystem(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected CoordinateSystem
		exists   bool
	}{
		{1, 99992229999, CoordinateSystem{}, false},
		{2, 0, CoordinateSystem{}, false},
		{3, 1024, CoordinateSystem{SRID: 1024, Name: "Cartesian 2D CS.", Type: "Cartesian", Dimension: 2, Deprecated: false}, true},
		{4, 1025, CoordinateSystem{SRID: 1025, Name: "Cartesian 2D CS for south polar azimuthal", Type: "Cartesian", Dimension: 2, Deprecated: false}, true},
		{5, 1033, CoordinateSystem{SRID: 1033, Name: "Bin grid CS", Type: "Cartesian", Dimension: 2, Deprecated: false}, true},
	}
	for _, tc := range testCases {
		cs, exists := SelectCoordinateSystem(tc.id)
		if exists != tc.exists {
			t.Errorf("SelectCoordinateSystem() test %d fail ! It must not exists", tc.n)
		}
		if exists {
			if cs.SRID != tc.id {
				t.Errorf("SelectCoordinateSystem() test %d fail ! Data SRID corrupted", tc.n)
			}
			if !strings.HasPrefix(cs.Name, tc.expected.Name) {
				t.Errorf("SelectCoordinateSystem() test %d fail ! Data Name corrupted", tc.n)
			}
			if cs.Type != tc.expected.Type {
				t.Errorf("SelectCoordinateSystem() test %d fail ! Data Type corrupted", tc.n)
			}
			if cs.Dimension != tc.expected.Dimension {
				t.Errorf("SelectCoordinateSystem() test %d fail ! Data Dimension corrupted", tc.n)
			}
			if cs.Deprecated != tc.expected.Deprecated {
				t.Errorf("SelectCoordinateSystem() test %d fail ! Data Deprecated corrupted", tc.n)
			}
		}
	}
}

func Test_SelectCoordinateOperation(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected CoordinateOperation
		exists   bool
	}{
		{1, 99992229999, CoordinateOperation{}, false},
		{2, 0, CoordinateOperation{}, false},
		{3, 101, CoordinateOperation{SRID: 101, Name: "deg to DMSH", Type: "conversion", AreaOfUse: Area{SRID: 1262}, Scope: "Convert decimal degrees to and from degrees minutes seconds hemisphere representation.", Accuracy: 0, Method: CoordinateOperationMethod{SRID: 9637}, Deprecated: true}, true},
		{4, 102, CoordinateOperation{SRID: 102, Name: "degH to DMSH", Type: "conversion", AreaOfUse: Area{SRID: 1262}, Scope: "Convert degree hemisphere representation to and from degrees minutes seconds hemisphere.", Accuracy: 0, Method: CoordinateOperationMethod{SRID: 9638}, Deprecated: true}, true},
	}
	for _, tc := range testCases {
		co, exists := SelectCoordinateOperation(tc.id)
		if exists != tc.exists {
			t.Errorf("SelectCoordinateOperation() test %d fail ! It must not exists", tc.n)
		}
		if exists {
			if co.SRID != tc.id {
				t.Errorf("SelectCoordinateOperation() test %d fail ! Data SRID corrupted", tc.n)
			}
			if co.Name != tc.expected.Name {
				t.Errorf("SelectCoordinateOperation() test %d fail ! Data Name corrupted", tc.n)
			}
			if co.Type != tc.expected.Type {
				t.Errorf("SelectCoordinateOperation() test %d fail ! Data Type corrupted", tc.n)
			}
			if co.AreaOfUse.SRID != tc.expected.AreaOfUse.SRID {
				t.Errorf("SelectCoordinateOperation() test %d fail ! Data AreaOfUse corrupted", tc.n)
			}
			if co.Scope != tc.expected.Scope {
				t.Errorf("SelectCoordinateOperation() test %d fail ! Data Scope corrupted", tc.n)
			}
			if co.Accuracy != tc.expected.Accuracy {
				t.Errorf("SelectCoordinateOperation() test %d fail ! Data Accuracy corrupted", tc.n)
			}
			if co.Method.SRID != tc.expected.Method.SRID {
				t.Errorf("SelectCoordinateOperation() test %d fail ! Data Method corrupted", tc.n)
			}
			if co.Deprecated != tc.expected.Deprecated {
				t.Errorf("SelectCoordinateOperation() test %d fail ! Data Deprecated corrupted", tc.n)
			}
		}
	}
}

func Test_SelectCoordinateOperationMethod(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected CoordinateOperationMethod
		exists   bool
	}{
		{1, 99992229999, CoordinateOperationMethod{}, false},
		{2, 0, CoordinateOperationMethod{}, false},
		{3, 1024, CoordinateOperationMethod{SRID: 1024, Name: "Popular Visualisation Pseudo Mercator", Reverse: true, Deprecated: false}, true},
		{4, 1025, CoordinateOperationMethod{SRID: 1025, Name: "Geographic3D to GravityRelatedHeight (EGM2008)", Reverse: false, Deprecated: false}, true},
	}
	for _, tc := range testCases {
		com, exists := SelectCoordinateOperationMethod(tc.id)
		if exists != tc.exists {
			t.Errorf("SelectCoordinateOperationMethod() test %d fail ! It must not exists", tc.n)
		}
		if exists {
			if com.SRID != tc.id {
				t.Errorf("SelectCoordinateOperationMethod() test %d fail ! Data SRID corrupted", tc.n)
			}
			if com.Name != tc.expected.Name {
				t.Errorf("SelectCoordinateOperationMethod() test %d fail ! Data Name corrupted", tc.n)
			}
			if com.Deprecated != tc.expected.Deprecated {
				t.Errorf("SelectCoordinateOperationMethod() test %d fail ! Data Deprecated corrupted", tc.n)
			}
		}
	}
}

func Test_SelectCoordinateOperationParameter(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected CoordinateOperationParameter
		exists   bool
	}{
		{1, 99992229999, CoordinateOperationParameter{}, false},
		{2, 0, CoordinateOperationParameter{}, false},
		{3, 1024, CoordinateOperationParameter{SRID: 1024, Name: "Cartesian coordinate programme file", Deprecated: false}, true},
		{4, 1025, CoordinateOperationParameter{SRID: 1025, Name: "GNTRANS transformation identifier", Deprecated: false}, true},
	}
	for _, tc := range testCases {
		cop, exists := SelectCoordinateOperationParameter(tc.id)
		if exists != tc.exists {
			t.Errorf("SelectCoordinateOperationParameter() test %d fail ! It must not exists", tc.n)
		}
		if exists {
			if cop.SRID != tc.id {
				t.Errorf("SelectCoordinateOperationParameter() test %d fail ! Data SRID corrupted", tc.n)
			}
			if cop.Name != tc.expected.Name {
				t.Errorf("SelectCoordinateOperationParameter() test %d fail ! Data Name corrupted", tc.n)
			}
			if cop.Deprecated != tc.expected.Deprecated {
				t.Errorf("SelectCoordinateOperationParameter() test %d fail ! Data Deprecated corrupted", tc.n)
			}
		}
	}
}

func Test_SelectCoordinateOperationParameterUsage(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected []CoordinateOperationParameterUsage
		nb       int
	}{
		{1, 99992229999, []CoordinateOperationParameterUsage{}, 0},
		{2, 0, []CoordinateOperationParameterUsage{}, 0},
		{3, 1040, []CoordinateOperationParameterUsage{
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1040}, Parameter: CoordinateOperationParameter{SRID: 1024}, Order: 1},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1040}, Parameter: CoordinateOperationParameter{SRID: 1025}, Order: 2},
		}, 2},
		{4, 1042, []CoordinateOperationParameterUsage{
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8811}, Order: 1},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8833}, Order: 2},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1036}, Order: 3},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8818}, Order: 4},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8819}, Order: 5},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8806}, Order: 6},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8807}, Order: 7},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8617}, Order: 8},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8618}, Order: 9},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1026}, Order: 10},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1027}, Order: 11},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1028}, Order: 12},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1029}, Order: 13},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1030}, Order: 14},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1031}, Order: 15},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1032}, Order: 16},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1033}, Order: 17},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1034}, Order: 18},
			CoordinateOperationParameterUsage{Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1035}, Order: 19},
		}, 19},
	}
	for _, tc := range testCases {
		err := false
		results, nb := SelectCoordinateOperationParameterUsage(tc.id)
		if nb != tc.nb {
			t.Errorf("SelectCoordinateOperationParameterUsage() test %d fail ! Array having bad number items ! ", tc.n)
			err = true
		}
		if !err {
			for _, copu_results := range results {
				if copu_results.Method.SRID != tc.id {
					t.Errorf("SelectCoordinateOperationParameterUsage() test %d fail ! Data Method corrupted", tc.n)
				}
				in_result := false
				for _, copu_expected := range tc.expected {
					if copu_results.Parameter.SRID == copu_expected.Parameter.SRID &&
						copu_results.Order == copu_expected.Order {
						in_result = true
						break
					}
				}
				if !in_result {
					t.Errorf("SelectCoordinateOperationParameterUsage() test %d fail ! Data Parameter %d or Order corrupted !", tc.n, copu_results.Parameter.SRID)
				}
			}
		}
	}
}

func Test_SelectCoordinateOperationParameterValue(t *testing.T) {
	testCases := []struct {
		n            int
		operation_id int
		method_id    int
		expected     []CoordinateOperationParameterValue
		nb           int
	}{
		{1, 99992229999, 9999, []CoordinateOperationParameterValue{}, 0},
		{2, 0, 0, []CoordinateOperationParameterValue{}, 0},
		{3, 5076, 9656, []CoordinateOperationParameterValue{CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5076}, Method: CoordinateOperationMethod{SRID: 9656}, Parameter: CoordinateOperationParameter{SRID: 8728}, Value: 357, Unit: Unit{SRID: 9001}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5076}, Method: CoordinateOperationMethod{SRID: 9656}, Parameter: CoordinateOperationParameter{SRID: 8729}, Value: 188, Unit: Unit{SRID: 9001}}}, 2},
		{4, 5219, 1042, []CoordinateOperationParameterValue{
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1026}, Value: 0.02946529277, Unit: Unit{SRID: 9203}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1027}, Value: 0.02515965696, Unit: Unit{SRID: 9203}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1028}, Value: 1.193845912E-07, Unit: Unit{SRID: 9203}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1029}, Value: -4.668270147E-07, Unit: Unit{SRID: 9203}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1030}, Value: 9.233980362E-12, Unit: Unit{SRID: 9203}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1031}, Value: 1.523735715E-12, Unit: Unit{SRID: 9203}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1032}, Value: 1.696780024E-18, Unit: Unit{SRID: 9203}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1033}, Value: 4.408314235E-18, Unit: Unit{SRID: 9203}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1034}, Value: -8.331083518E-24, Unit: Unit{SRID: 9203}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1035}, Value: -3.689471323E-24, Unit: Unit{SRID: 9203}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 1036}, Value: 30.1717303, Unit: Unit{SRID: 9110}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8617}, Value: 1089000, Unit: Unit{SRID: 9001}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8618}, Value: 654000, Unit: Unit{SRID: 9001}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8806}, Value: 5000000, Unit: Unit{SRID: 9001}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8807}, Value: 5000000, Unit: Unit{SRID: 9001}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8811}, Value: 49.3, Unit: Unit{SRID: 9110}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8818}, Value: 78.3, Unit: Unit{SRID: 9110}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8819}, Value: 0.9999, Unit: Unit{SRID: 9201}},
			CoordinateOperationParameterValue{Operation: CoordinateOperation{SRID: 5219}, Method: CoordinateOperationMethod{SRID: 1042}, Parameter: CoordinateOperationParameter{SRID: 8833}, Value: 42.3, Unit: Unit{SRID: 9110}},
		}, 19},
	}
	for _, tc := range testCases {
		err := false
		results, nb := SelectCoordinateOperationParameterValue(tc.operation_id, tc.method_id)
		if nb != tc.nb {
			t.Errorf("SelectCoordinateOperationParameterValue() test %d fail ! Array having bad number items ! ", tc.n)
			err = true
		}
		if !err {
			for _, copv_results := range results {
				if copv_results.Operation.SRID != tc.operation_id || copv_results.Method.SRID != tc.method_id {
					t.Errorf("SelectCoordinateOperationParameterValue() test %d fail ! Data Operation or Method corrupted", tc.n)
				}
				in_result := false
				for _, copv_expected := range tc.expected {
					if copv_results.Parameter.SRID == copv_expected.Parameter.SRID &&
						copv_results.Value == copv_expected.Value {
						in_result = true
						break
					}
				}
				if !in_result {
					t.Errorf("SelectCoordinateOperationParameterValue() test %d fail ! Data Operation %d and Method %d having data corrupted !", tc.n, copv_results.Operation.SRID, copv_results.Method.SRID)
				}
			}
		}
	}
}

func Test_SelectCoordinateReferenceSystem(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected CoordinateReferenceSystem
		exists   bool
	}{
		{1, 99992229999, CoordinateReferenceSystem{}, false},
		{2, 0, CoordinateReferenceSystem{}, false},
		{3, 3819, CoordinateReferenceSystem{SRID: 3819, Name: "HD1909", AreaOfUse: Area{SRID: 1119}, Type: "geographic 2D", CoordinateSystem: CoordinateSystem{SRID: 6422}, Datum: Datum{SRID: 1024}, GeographicCrsSource: 0, ProjectionConversion: 0, Deprecated: false}, true},
		{4, 3821, CoordinateReferenceSystem{SRID: 3821, Name: "TWD67", AreaOfUse: Area{SRID: 3315}, Type: "geographic 2D", CoordinateSystem: CoordinateSystem{SRID: 6422}, Datum: Datum{SRID: 1025}, GeographicCrsSource: 0, ProjectionConversion: 0, Deprecated: false}, true},
	}
	for _, tc := range testCases {
		crs, exists := SelectCoordinateReferenceSystem(tc.id)
		if exists != tc.exists {
			t.Errorf("SelectCoordinateReferenceSystem() test %d fail ! It must not exists", tc.n)
		}
		if exists {
			if crs.SRID != tc.id {
				t.Errorf("SelectCoordinateReferenceSystem() test %d fail ! Data SRID corrupted", tc.n)
			}
			if crs.Name != tc.expected.Name {
				t.Errorf("SelectCoordinateReferenceSystem() test %d fail ! Data Name corrupted", tc.n)
			}
			if crs.AreaOfUse.SRID != tc.expected.AreaOfUse.SRID {
				t.Errorf("SelectCoordinateReferenceSystem() test %d fail ! Data AreaOfUse corrupted", tc.n)
			}
			if crs.Type != tc.expected.Type {
				t.Errorf("SelectCoordinateReferenceSystem() test %d fail ! Data Type corrupted", tc.n)
			}
			if crs.CoordinateSystem.SRID != tc.expected.CoordinateSystem.SRID {
				t.Errorf("SelectCoordinateReferenceSystem() test %d fail ! Data CoordinateSystem corrupted", tc.n)
			}
			if crs.Datum.SRID != tc.expected.Datum.SRID {
				t.Errorf("SelectCoordinateReferenceSystem() test %d fail ! Data Datum corrupted", tc.n)
			}
			if crs.ProjectionConversion != tc.expected.ProjectionConversion {
				t.Errorf("SelectCoordinateReferenceSystem() test %d fail ! Data ProjectionConversion corrupted", tc.n)
			}
			if crs.GeographicCrsSource != tc.expected.GeographicCrsSource {
				t.Errorf("SelectCoordinateReferenceSystem() test %d fail ! Data GeographicCrsSource corrupted", tc.n)
			}
			if crs.Deprecated != tc.expected.Deprecated {
				t.Errorf("SelectCoordinateReferenceSystem() test %d fail ! Data Deprecated corrupted", tc.n)
			}
		}
	}
}

func Test_SelectDatum(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected Datum
		exists   bool
	}{
		{1, 99992229999, Datum{}, false},
		{2, 0, Datum{}, false},
		{3, 1024, Datum{SRID: 1024, Name: "Hungarian Datum 1909", Type: "geodetic", Ellipsoid: Ellipsoid{SRID: 7004}, PrimeMeridian: PrimeMeridian{SRID: 8901}, AreaOfUse: Area{SRID: 1119}, Deprecated: false}, true},
		{4, 1025, Datum{SRID: 1025, Name: "Taiwan Datum 1967", Type: "geodetic", Ellipsoid: Ellipsoid{SRID: 7050}, PrimeMeridian: PrimeMeridian{SRID: 8901}, AreaOfUse: Area{SRID: 3315}, Deprecated: false}, true},
	}
	for _, tc := range testCases {
		d, exists := SelectDatum(tc.id)
		if exists != tc.exists {
			t.Errorf("SelectDatum() test %d fail ! Must returns don't exists.", tc.n)
		}
		if exists {
			if d.SRID != tc.id {
				t.Errorf("SelectDatum() test %d fail ! Data SRID corrupted", tc.n)
			}
			if d.Name != tc.expected.Name {
				t.Errorf("SelectDatum() test %d fail ! Data Name corrupted", tc.n)
			}
			if d.Type != tc.expected.Type {
				t.Errorf("SelectDatum() test %d fail ! Data Type corrupted", tc.n)
			}
			if d.Ellipsoid.SRID != tc.expected.Ellipsoid.SRID {
				t.Errorf("SelectDatum() test %d fail ! Data Epoch corrupted", tc.n)
			}
			if d.PrimeMeridian.SRID != tc.expected.PrimeMeridian.SRID {
				t.Errorf("SelectDatum() test %d fail ! Data PrimeMeridian corrupted", tc.n)
			}
			if d.AreaOfUse.SRID != tc.expected.AreaOfUse.SRID {
				t.Errorf("SelectDatum() test %d fail ! Data AreaOfUse corrupted", tc.n)
			}
			if d.Deprecated != tc.expected.Deprecated {
				t.Errorf("SelectDatum() test %d fail ! Data Deprecated corrupted", tc.n)
			}
		}
	}
}

func Test_SelectEllipsoid(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected Ellipsoid
		exists   bool
	}{
		{1, 99992229999, Ellipsoid{}, false},
		{2, 0, Ellipsoid{}, false},
		{3, 1024, Ellipsoid{SRID: 1024, Name: "CGCS2000", SemiMajorAxis: 6378137, Unit: Unit{SRID: 9001}, Flattening: 0.00335281068118231879, InvFlattening: 298.25722210100002484978, SemiMinorAxis: 6356752.31414035614579916000, Eccentricity: 0.08181919104281579203, SecondEccentricity: 0.08209443815191719285, IsSphere: true, Deprecated: false}, true},
		{4, 7001, Ellipsoid{SRID: 7001, Name: "Airy 1830", SemiMajorAxis: 6377563.396, Unit: Unit{SRID: 9001}, Flattening: 0.00334085064149707749, InvFlattening: 299.32496459999998705825, SemiMinorAxis: 6356256.90923728514462709427, Eccentricity: 0.08167337387414189132, SecondEccentricity: 0.08194714705294256496, IsSphere: true, Deprecated: false}, true},
	}

	for _, tc := range testCases {
		e, exists := SelectEllipsoid(tc.id)
		if exists != tc.exists {
			t.Errorf("SelectEllipsoid() test %d fail ! Must returns don't exists.", tc.n)
		}
		if exists {
			if e.SRID != tc.id {
				t.Errorf("SelectEllipsoid() test %d fail ! Data SRID corrupted", tc.n)
			}
			if e.Name != tc.expected.Name {
				t.Errorf("SelectEllipsoid() test %d fail ! Data Name corrupted", tc.n)
			}
			if e.SemiMajorAxis != tc.expected.SemiMajorAxis {
				t.Errorf("SelectEllipsoid() test %d fail ! Data SemiMajorAxis corrupted", tc.n)
			}
			if e.Unit.SRID != tc.expected.Unit.SRID {
				t.Errorf("SelectEllipsoid() test %d fail ! Data Unit corrupted", tc.n)
			}
			if e.Flattening != tc.expected.Flattening {
				t.Errorf("SelectEllipsoid() test %d fail ! Data Flattening corrupted", tc.n)
			}
			if e.InvFlattening != tc.expected.InvFlattening {
				t.Errorf("SelectEllipsoid() test %d fail ! Data InvFlattening corrupted", tc.n)
			}
			if e.SemiMinorAxis != tc.expected.SemiMinorAxis {
				t.Errorf("SelectEllipsoid() test %d fail ! Data SemiMinorAxis corrupted", tc.n)
			}
			if e.Eccentricity != tc.expected.Eccentricity {
				t.Errorf("SelectEllipsoid() test %d fail ! Data Eccentricity corrupted", tc.n)
			}
			if e.SecondEccentricity != tc.expected.SecondEccentricity {
				t.Errorf("SelectEllipsoid() test %d fail ! Data SecondEccentricity corrupted", tc.n)
			}
			if e.IsSphere != tc.expected.IsSphere {
				t.Errorf("SelectEllipsoid() test %d fail ! Data IsSphere corrupted", tc.n)
			}
			if e.Deprecated != tc.expected.Deprecated {
				t.Errorf("SelectEllipsoid() test %d fail ! Data Deprecated corrupted", tc.n)
			}
		}
	}
	// Test part GIGS 2000 Pre-defined geodetic parameter test (data quality)
	// adapted from GIGS_2002_libEllipsoid_v2-0_2011-06-28.xls
	testData := []struct {
		id            int
		name          string
		semiMajor     float64
		invFlattening float64
		semiMinor     float64
		isSphere      bool
	}{
		{7001, "Airy 1830", 6377563.396, 299.3249646, 0, false},
		{7002, "Airy Modified 1849", 6377340.189, 299.3249646, 0, false},
		{7003, "Australian National Spheroid", 6378160, 298.25, 0, false},
		{7004, "Bessel 1841", 6377397.155, 299.1528128, 0, false},
		{7046, "Bessel Namibia (GLM)", 6377397.155, 299.1528128, 0, false},
		{7007, "Clarke 1858", 20926348, 0, 20855233, false},
		{7008, "Clarke 1866", 6378206.4, 0, 6356583.8, false},
		{7009, "Clarke 1866 Michigan", 20926631.531, 0, 20855688.674, false},
		{7011, "Clarke 1880 (IGN)", 6378249.2, 0, 6356515, false},
		{7012, "Clarke 1880 (RGS)", 6378249.145, 293.465, 0, false},
		{7015, "Everest 1830 (1937 Adjustment)", 6377276.345, 300.8017, 0, false},
		{7044, "Everest 1830 (1962 Definition)", 6377301.243, 300.8017255, 0, false},
		{7016, "Everest 1830 (1967 Definition)", 6377298.556, 300.8017, 0, false},
		{7045, "Everest 1830 (1975 Definition)", 6377299.151, 300.8017255, 0, false},
		{7018, "Everest 1830 Modified", 6377304.063, 300.8017, 0, false},
		{7036, "GRS 1967", 6378160, 298.247167427, 0, false},
		{7050, "GRS 1967", 6378160, 298.25, 0, false},
		{7019, "GRS 1980", 6378137, 298.257222101, 0, false},
		{7020, "Helmert 1906", 6378200, 298.3, 0, false},
		{7021, "Indonesian National Spheroid", 6378160, 298.247, 0, false},
		{7022, "International 1924", 6378388, 297, 0, false},
		{7024, "Krassowsky 1940", 6378245, 298.3, 0, false},
		{7029, "War Office", 6378300, 296, 0, false},
		{7043, "WGS 72", 6378135, 298.26, 0, false},
		{7030, "WGS 84", 6378137, 298.257223563, 0, false},
		{7049, "IAG 1975", 6378140, 298.257, 0, false},
		{7041, "Average Terrestrial System 1977", 6378135, 298.257, 0, false},
		{7005, "Bessel Modified", 6377492.018, 299.1528128, 0, false},
		{7052, "Clarke 1866 Authalic Sphere", 6370997, 0, 6370997, true},
		{7034, "Clarke 1880", 20926202, 0, 20854895, false},
		{7013, "Clarke 1880 (Arc)", 6378249.145, 293.4663077, 0, false},
		{7010, "Clarke 1880 (Benoit)", 6378300.789, 0, 6356566.435, false},
		{7055, "Clarke 1880 (international foot)", 20926202, 0, 20854895, false},
		{7014, "Clarke 1880 (SGA 1922)", 6378249.2, 293.46598, 0, false},
		{7051, "Danish 1876", 6377019.27, 300, 0, false},
		{7042, "Everest (1830 Definition)", 20922931.8, 0, 20853374.58, false},
		{7056, "Everest 1830 (RSO 1969)", 6377295.664, 300.8017, 0, false},
		{7031, "GEM 10C", 6378137, 298.257223563, 0, false},
		{7048, "GRS 1980 Authalic Sphere", 6371007, 0, 6371007, true},
		{7053, "Hough 1960", 6378270, 297, 0, false},
		{7058, "Hughes 1980", 6378273, 0, 6356889.449, true},
		{7057, "International 1924 Authalic Sphere", 6371228, 0, 6371228, true},
		{7025, "NWL 9D", 6378145, 298.25, 0, false},
		{7032, "OSU86F", 6378136.2, 298.257223563, 0, false},
		{7033, "OSU91A", 6378136.3, 298.257223563, 0, false},
		{7027, "Plessis 1817", 6376523, 308.64, 0, false},
		{7059, "Popular Visualisation Sphere", 6378137, 0, 6378137, true},
		{7054, "PZ-90", 6378136, 298.257839303, 0, false},
		{7028, "Struve 1860", 6378298.3, 294.73, 0, false},
	}
	for _, td := range testData {
		e, exists := SelectEllipsoid(td.id)
		if !exists {
			t.Errorf("Test data quality fail with unknown unit SRID=%d!", td.id)
		} else {
			if e.SRID != td.id {
				t.Errorf("SelectEllipsoid() test data quality fail with bad SRID on ellipsoid SRID=%d!", td.id)
			}
			if !strings.HasPrefix(e.Name, td.name) {
				t.Errorf("SelectEllipsoid() test data quality fail with bad name on ellipsoid SRID=%d!"+
					"expect --> %s, but read --> %s", td.id, td.name, e.Name)
			}
			if e.SemiMajorAxis != td.semiMajor {
				t.Errorf("SelectEllipsoid() test data quality fail with bad semi major axis on ellipsoid SRID=%d!"+
					"expect --> %g, but read --> %g", td.id, td.semiMajor, e.SemiMajorAxis)
			}
			if td.invFlattening > 0 {
				if e.InvFlattening != td.invFlattening {
					t.Errorf("SelectEllipsoid() test data quality fail with bad inverse flattening on ellipsoid SRID=%d!"+
						"expect --> %g, but read --> %g", td.id, td.invFlattening, e.InvFlattening)
				}
			} else {
				if e.SemiMinorAxis != td.semiMinor {
					t.Errorf("SelectEllipsoid() test data quality fail with bad semi minor axis on ellipsoid SRID=%d!"+
						"expect --> %g, but read --> %g", td.id, td.semiMinor, e.SemiMinorAxis)
				}
			}
		}
	}
}

func Test_SelectPrimeMeridian(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected PrimeMeridian
		exists   bool
	}{
		{1, 99992229999, PrimeMeridian{}, false},
		{2, 0, PrimeMeridian{}, false},
		{3, 8901, PrimeMeridian{SRID: 8901, Name: "Greenwich", GreenwichLongitude: 0, Unit: Unit{SRID: 9102}, Deprecated: false}, true},
		{4, 8902, PrimeMeridian{SRID: 8902, Name: "Lisbon", GreenwichLongitude: -9.0754862, Unit: Unit{SRID: 9110}, Deprecated: false}, true},
	}

	for _, tc := range testCases {
		pm, exists := SelectPrimeMeridian(tc.id)
		if exists != tc.exists {
			t.Errorf("SelectPrimeMeridian() test %d fail ! Must returns don't exists.", tc.n)
		}
		if exists {
			if pm.SRID != tc.id {
				t.Errorf("SelectPrimeMeridian() test %d fail ! Data SRID corrupted", tc.n)
			}
			if pm.Name != tc.expected.Name {
				t.Errorf("SelectPrimeMeridian() test %d fail ! Data Name corrupted", tc.n)
			}
			if pm.GreenwichLongitude != tc.expected.GreenwichLongitude {
				t.Errorf("SelectPrimeMeridian() test %d fail ! Data GreenwichLongitude corrupted", tc.n)
			}
			if pm.Unit.SRID != tc.expected.Unit.SRID {
				t.Errorf("SelectPrimeMeridian() test %d fail ! Data Unit corrupted", tc.n)
			}
			if pm.Deprecated != tc.expected.Deprecated {
				t.Errorf("SelectPrimeMeridian) test %d fail ! Data Deprecated corrupted", tc.n)
			}
		}
	}
}

func Test_SelectUnit(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected Unit
		exists   bool
	}{
		{1, 99992229999, Unit{}, false},
		{2, 0, Unit{}, false},
		{3, 1024, Unit{SRID: 1024, Name: "bin", Type: "scale", TargetUnitSRID: 9201, FactorB: 1, FactorC: 1, Deprecated: false}, true},
		{4, 1025, Unit{SRID: 1025, Name: "millimetre", Type: "length", TargetUnitSRID: 9001, FactorB: 1, FactorC: 1000, Deprecated: false}, true},
	}
	for _, tc := range testCases {
		u, exists := SelectUnit(tc.id)
		if tc.exists != exists {
			t.Errorf("SelectUnit() test %d fail ! Must returns don't exists.", tc.n)
		}
		if exists {
			if u.SRID != tc.id {
				t.Errorf("SelectUnit() test %d fail ! Data SRID corrupted", tc.n)
			}
			if u.Name != tc.expected.Name {
				t.Errorf("SelectUnit() test %d fail ! Data Name corrupted", tc.n)
			}
			if u.Type != tc.expected.Type {
				t.Errorf("SelectUnit() test %d fail ! Data Type corrupted", tc.n)
			}
			if u.TargetUnitSRID != tc.expected.TargetUnitSRID {
				t.Errorf("SelectUnit() test %d fail ! Data TargetUnitSRID corrupted", tc.n)
			}
			if u.FactorB != tc.expected.FactorB {
				t.Errorf("SelectUnit() test %d fail ! Data FactorB corrupted", tc.n)
			}
			if u.FactorC != tc.expected.FactorC {
				t.Errorf("SelectUnit() test %d fail ! Data FactorC corrupted", tc.n)
			}
			if u.Deprecated != tc.expected.Deprecated {
				t.Errorf("SelectUnit() test %d fail ! Data Deprecated corrupted", tc.n)
			}
		}
	}
	// Test part GIGS 2000 Pre-defined geodetic parameter test (data quality)
	// adapted from GIGS_2001_libUnit_v2-0_2011-06-28.xls
	testData := []struct {
		id    int
		name  string
		ratio float64
	}{
		{9001, "metre", 1},
		{9036, "kilometre", 1000},
		{9002, "foot", 0.3048},
		{9003, "US survey foot", 0.30480061},
		{9031, "German legal metre", 1.000013597},
		{9005, "Clarke", 0.304797265},
		{9039, "Clarke", 0.201166195},
		{9042, "British chain (Sears 1922)", 20.11676512},
		{9051, "British foot", 0.3047997},
		{9040, "British yard (Sears 1922)", 0.914398415},
		{9301, "British chain (Sears 1922 truncated)", 20.116756},
		{9084, "Indian yard", 0.914398531},
		{9094, "Gold Coast foot", 0.30479971},
		{9098, "link", 0.201168},
		{9101, "radian", 1},
		{9102, "degree", 0.017453293},
		{9104, "arc-second", 4.848136811095355e-06}, //.4.85E-06
		{9105, "grad", 0.015707963},
		{9109, "microradian", 0.000001},
		{9110, "sexagesimal DMS", 0},                       // NULL
		{9113, "centesimal second", 1.570796326794895e-06}, //1.57E-06
		{9201, "unity", 1},
		{9202, "parts per million", 0.000001},
		{9203, "coefficient", 1}, // NULL
	}
	for _, td := range testData {
		u, exists := SelectUnit(td.id)
		if !exists {
			t.Errorf("SelectUnit() Test data quality fail with unknown unit SRID=%d!", td.id)
		} else {
			if u.SRID != td.id {
				t.Errorf("SelectUnit() Test data quality fail with bad SRID on unit SRID=%d!", td.id)
			}
			if !strings.HasPrefix(u.Name, td.name) {
				t.Errorf("SelectUnit() Test data quality fail with bad name on unit SRID=%d!"+
					"expect --> %s, but read --> %s", td.id, td.name, u.Name)
			}
			ratio := u.FactorB / u.FactorC
			if math.Abs(ratio-td.ratio) > 0.0000001 {
				t.Errorf(
					"SelectUnit() Test data quality fail with bad conversion ratio on unit SRID=%d!"+
						"expect --> %g, but compute --> %g", td.id, td.ratio, ratio)
			}
		}
	}
}
