/*
core_test.go is part of Cassini project.

Copyright (C) 2015 Eric Boulet

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package srs

import (
	"testing"
)

//------------------------------------------------------------------------------
// Unit tests
//------------------------------------------------------------------------------

func Test_Unit_ToWKT(t *testing.T) {
	testCases := []struct {
		n        int
		id       int
		expected string
	}{
		{1, 9001, "UNIT [\"metre\", 1]"},
		{2, 9002, "UNIT [\"foot\", 0.3048]"},
		{3, 9101, "UNIT [\"radian\", 1]"},
	}
	for _, tc := range testCases {
		u, exists := SelectUnit(tc.id)
		if !exists {
			t.Error("Unit.ToWKT() test fail during prestatment !")
		} else {
			if u.ToWKT() != tc.expected {
				t.Errorf("Unit.ToWKT() test %d fail ! Result %s --> expected %s",
					tc.n,
					u.ToWKT(),
					tc.expected)
			}
		}
	}
}
